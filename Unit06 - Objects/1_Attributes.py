# create Person OBJECT
class Person:
    def __init__(self,n,a):
        self.name = n
        self.age = a
        self.__age = a

    def birthday(self):
        self.age += 1
        self.__age += 1

# create an INSTANCE of Person OBJECT
john = Person("John Smith", 27)
# access Person's age (ATTRIBUTE)
print(john.age)
# call Person's birthday (Method)
john.birthday()
john.birthday()
# access Person's age again...
print(john.age)



# "__" means the attribute/method is "private"
# "private" things can only
# be refered to WITHIN the class!
# If we try to access something 
# private outside of the class
# the program will crash, eg:
print(john.__age)


# programmers use "private" as a way to
# prevent other programmers on their team
# from messing with code that
# is not their responsiblity