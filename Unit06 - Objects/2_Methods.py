# make sure to touch on OVERLOADING
class Person:
    def __init__(self,n,a):
        self.name = n
        self.age = a
        self.__age = a

    def birthday(self, times=1):
        self.age += 1
        self.__age += 1
        print(self.age)

    def say(self,speech):
        print(f"{self.name} says...")
        print(f"\t\"{speech}\"")
        return len(speech)

j = Person("John Smith", 27)
j.birthday()
j.birthday(6)

length = j.say("Well look at that, I'm an object.")
print(length)