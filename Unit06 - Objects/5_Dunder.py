# DUNDER is short for "double underscore"
# It refers to METHODS that start and end with "__"
# All objects can have thier DUNDER METHODS POLYMORPHED 


class example:

    # what this OBJECT does to initialize itself
    def __init__(self, c):
        self.color = "red"
        self.log = []

    # what this OBJECT returns when indexed like a list or dictionary
    def __getitem__(self,index):
        return self.log[index]

    # what this OBJECT returns when cast to a string with str()
    def __str__(self):
        return self.color


# There are TONS of DUNDER methods
# refer to (Python-Dunder-Cheat-Sheet.html)