# Decorators provide enhanced functionality to a function (or class) 
# we "decorate" our code with an @ followed by a decorator name
# Python has some great built-in decorators!
# Here are some really useful built-in decorators
# that make writing OOP code easier

class ExampleClass:
    def __init__(self):
        self.__score = 0
        self.__total = 0

    # read/write property
    @property
    def score(self):
        return self.__score
    @score.setter
    def score(self, value):
        self.__score = value

    # read only property
    @property
    def total(self):
        return self.__total

    @classmethod
    def boom(cls):
        print("boom")
        print(cls)
        # ^^^ reference to parent class
        # (we can use classmethods for Factory Pattern)
        # (complex/alternate object constructors)

    @staticmethod
    def clack():
        print("clack")
        # no reference to parent class...
        # (identical to having the function outside of class)
        # (we use this organization, not functionality!)


exampleInstance = ExampleClass()

# instance propery (read/write)
exampleInstance.score += 1
print(exampleInstance.score)

# instance propety (read only)
print(exampleInstance.total)


# class method (does not require an instance)
ExampleClass.boom()

# static method (does not require an instance)
ExampleClass.clack()


