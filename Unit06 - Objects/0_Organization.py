'''
Up until this point we have been oranizing our code by breaking things down into functions.
however, you may have noticed that many functions and variables can be logically organized into "groups"
You may have simply been naming this functions and variables something similar up until this point.
These "groups" of code can be formally organized in our code using "Object Oriented Programming"


The main point of OOP is to organize our code in logical groups
There are 3 goals OOP tries to provide
    make code easy to read (reads like a sentence)
    make it easy to find specific code in large projects
    maximize code reusage (dont copy paste, utilize objects instead)

OOP provides us with A LOT of features / ways to do the same thing.
Most programmers use OOP wrong and incorrectly conclude OOP is bad.
The truth is those same programmers have never studied "Object Oriented Design"


OOD refers to the ways people should utilize OOP.
OOD refers to memorising and correctly deploying "Design Patterns" 
Design Patterns are tried and proven ways people have used OOP concepts in large projects.
We will not be getting into OOD... it is a "senior" level concept.

Think of OOP as our tools, and OOD as tricks of the trade.
First we need to know what the tools do before we start using them properly.


NOTE:
OOP is situational!
Just know that OOP/OOD makes sense for certain projects.
Single file programs are still great for short assignments / simple programs.
'''