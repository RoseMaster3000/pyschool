# a "dataclass" is just an object used to store data
# we can import the decorator needed to 
# create a dataclass as follows:


from dataclasses import dataclass

@dataclass
class DataClassCard:
    rank: str
    suit: str


# above code is just a shorthand / conscise way of writing
# the following:

class RegularCard:
    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit
