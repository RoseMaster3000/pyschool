# (Decorators themselves are functions or classes)
# We can write our own custom decorators



# "f" refers to the function being decorated
def grade_decorator(f):
    def wrapper(score, total):
        percent = f(score, total)
        grades = {
            5: 'A',
            4: 'A',
            3: 'B',
            2: 'C',
            1: 'D'
        }
        return f"{percent}%", grades[percent // 20]
    return wrapper


# example usage 
# (note, you can chain decorators!)
class Student:
    @staticmethod
    def get_percent(score, total):
        return round(score / total * 100, 2)

    @staticmethod
    @grade_decorator
    def get_percent_grade(score, total):
        return round(score / total * 100, 2)


grade = Student.get_percent(52, 60)
print(grade)

grade = Student.get_percent_grade(52, 60)
print(grade)