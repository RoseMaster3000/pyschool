from SQLiteConnector import querySQL


def createTable():
    query = (
        "CREATE TABLE IF NOT EXISTS users("
        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "username TEXT NOT NULL,"
        "hash TEXT NOT NULL"
        ");"
    )
    querySQL(query)


def getTableList():
    query = (
        "SELECT name FROM sqlite_master "
        "WHERE type='table';"
    )
    x = querySQL(query)
    print(x)


def getUsers():
    x = querySQL("SELECT * FROM users;")
    print(x)


def addUser():
    query = (
        "INSERT INTO users"
        "(username, hash)"
        "VALUES"
        "(?, ?);"
    )
    data = ("CoolGirl", "qqqqqqqqqqqqqq")
    querySQL(query, data)   


if __name__=="__main__":
    # createTable()
    # getTableList()
    addUser()
    getUsers()


