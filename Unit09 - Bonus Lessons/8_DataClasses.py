from dataclasses import dataclass
from dataclasses import astuple, asdict, field

# dataclass +> (eq, init, repr)
# frozen +> read-only/immutable (hash, setattr)
# order +> equalities (gt,lt,ge,le)
# slots +> have python store attributes in an array, not dict
#          (uses less memory!)
#          (means `self.__dict__` DOES NOT EXISTS)
#          (only works it attribute list is predetermined / never changes)
#          (only works in python 3.10+)
@dataclass(frozen=True, order=True, slots=False)
class Comment:
    id: int
    text: str = field(default="")
    replies: list[int] = field(
        default_factory=list,
        compare=False,
        hash=False,
        repr=False
        )

# fetch attributes from dataclass instance
comment = Comment(1, "Wow, this is pretty cool!")
print(comment)
print(comment.id)
print(astuple(comment))
print(asdict(comment))


# modify frozen (read-only/immutable) dataclass instance
from dataclasses import replace as dataReplace
dataReplace(comment, text="Editted my comment lol")


# inspect dataclass object
import inspect
from pprint import pprint
pprint(inspect.getmembers(Comment, inspect.isfunction))