# arguments of a function or method
# can be OPTIONAL

# This means that argument has 
# a DEFAULT value it falls back on
# when the argument is not given

def getPortionSize(total=100, headCount=2):
    return total / headCount

getPortionSize()
getPortionSize(10)
getPortionSize(10,3)


# All REQUIRED arguments must be given in order
# ...then optional arguments must be given in order
def getPortionSize(total, headCount=2, sloppiness=0):
    return (total-sloppiness) / headCount

getPortionSize(2, 10, 1)


# But if you want to specify a certain 
# optional argument, simply use NAMED ARGUMENTS
getPortionSize(2, sloppiness=1)
