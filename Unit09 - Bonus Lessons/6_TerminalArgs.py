# when you run the python script from a terminal
# you can provide data in the form of system arguments
# eg you can run this in the terminal
# _> python 6_TerminalArgs.py 30 122
# this will run this script, which will add 30 and 122

import sys
num1 = sys.argv[1]
num2 = sys.argv[2]

summation = int(num1) + int(num2)
print(f"The sum is {summation}")