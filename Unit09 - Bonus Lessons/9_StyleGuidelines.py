'''
PYTHON MANTRA:
Beautiful is better than ugly.
Explicit  is better than implicit.
Simple    is better than complex.
-------------------------------

What is Beautiful?
there are official standards that constitutes
how to write "beautiful" python code (PEP 8)

Theses standards include...

The maximum character width a line of python code should be
    78 characters for code
    72 characters for comments & doc strings
    (split strings with "\" or implicit joins)
    (split deeply nested loops into functions)
    (etc...)

How to do indentation
    indentation should be done with 4 spaces

Where/when to place blank lines
    top-level function and class definitions
        surround with 2 blank lines.
    Method definitions inside a class
        surround with 1 blank line.
    Extra blank lines may be used (sparingly)
        separate groups of related functions.
        separate logical sections within a function

And much, much more...
    read more about it here:
    https://peps.python.org/pep-0008/

'''