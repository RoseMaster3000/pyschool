# You can use **kwargs to let your functions
# take an arbitrary number of keyword arguments
# KWARGS == "keyword arguments"


def printAll(**kwargs):
    for key, value in kwargs.iteritems():
        print(f"{key} ~ {value}")

def printNice(header, **kwargs):
    print(header.upper())
    for key, value in kwargs.iteritems():
        print(f"{key} ~ {value}")

printAll(first_name="John", last_name="Doe")
print("---")
printNice("attention!", person="Jane", reason="payment overdue")


