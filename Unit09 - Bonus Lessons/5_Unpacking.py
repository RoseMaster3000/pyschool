# TUPLES can be explicitly UNPACKED
fruits = ("apple", "banana", "cherry")
(green, yellow, red) = fruits
g,y,r = fruits


# TUPLES can be implicitly UNPACKED 
# (Functions can return multiple values)
def calculation(a,b,c,d,e):
    x = a+b
    y = c+d-e
    return x,y
first, second = calculation(1,2,3,4,5)


# TUPLES can be UNPACKED into the
# arguments of a function
data = (9,76,2,1,20)
first, second = calculation(*data)


# DICTIONARIES can be UNPACKED into the
# keyword arguments (KWARGS) of a function
dataDict = {
    "c":4,
    "a":34,
    "b":3,
    "e":69,
    "d":-100,
} 
first, second = calculation(**dataDict)


