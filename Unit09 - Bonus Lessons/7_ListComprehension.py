# its pretty common for us to create new
# arrays based on old arrays

# for example, here is us making a new 
# list which is just an older list but
# every element is multiplied by 2...

someList = [1,2,3,4,5,6,7,8]
newList = []
for item in someList:
    newList.append(item*2)


# This is such a common thing you will
# do, that python provides a faster way 
# for us to do this called "List Comprehension"

someList = [1,2,3,4,5,6,7,8]
newList = [item*2 for item in someList]


########################################
# You might build a list but with an if
# statment to "filter" it...
# for example, here is us making a list 
# with only the even numbers

someList = [1,2,3,4,5,6,7,8]
newList = []
for item in someList:
    if item%2==0:
        newList.append(item)


# We can rewrite this with list comprehension as well!

someList = [1,2,3,4,5,6,7,8]
newList = [item for item in someList if item%2==0]

