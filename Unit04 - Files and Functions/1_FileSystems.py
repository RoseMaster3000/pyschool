'''
data stored in VARIABLES in a program is that it is EPHEMERAL
The data only exists during RUNTIME
When the program is done, all the data is gone! forever!
If you lose power: gone.
If you accidentally close the program: gone.
Launch a program a second time? All the data from the first time: gone!

This is why we store/read data from files!

before we code reading/writing files...
its important to understand file fundamentals first

───────────────────────────────

Files and DIRECTORIES are stored in a TREE
🐧    https://rb.gy/neaj5j
🪟    https://rb.gy/wofwwu
> tree

Python Class 📁
  ├── agenda.txt
  ├── syllabus.pdf
  ├── Unit1 📁
  │   ├── Expressions.py
  │   ├── Variables.py
  │   ├── Characters.py
  │   ├── Strings.py
  │   ├── Programs 📁
  │   │   ├── homework.py
  │   │   ├── speedometer.py
  │   │   └── taxes.py
  │   └── Vocab.txt
  └── Unit2 📁
      ├── BooleanOperators.py
      ├── Conditionals.py
      ├── Loops.py
      ├── List.py
      └── Dictionary.py

───────────────────────────────

The ROOT is the very bottom of folder struture
🐧    /
🪟    C:\


ABSOLUTE PATH starts at the ROOT
🐧     /home/shahrose/Documents/PythonTutoring/fileio.py
🪟     C:\\Users\\shahrose\\Documents\\PythonTutoring\\fileio.py


RELATIVE PATH starts relative to a folder you are inside
(same file, relative to the Documents folder...)
🐧    PythonTutoring/fileio.py
🪟    PythonTutoring\\fileio.py


'''