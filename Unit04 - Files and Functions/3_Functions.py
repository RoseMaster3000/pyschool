# Files have gone chronologically (top to bottom)
# Thats about to change...
# Functions let us bundle a chunk of code
# and refer to that chunk by a "name"

# sort of like how a variable stores data
# a function stores code


def divide(numerator, denominator):
    quotient = numerator / denominator
    return quotient

n = 10
d = 2
answer = divide(n,d)
print(answer)

# When you call a function, its opens 
# its own "pocket dimention" with its
# own variables separate from the rest
# of the world

# You pass in data into a function via
# its ARGUEMENTS.
# In this case, divide has 2 arguments
# "numerator" and "denominator"



# You can specify the data types for
# the arguments of your functions
# as well as the output

def multiply(a:int, b:int, name:str) -> int:
    product = a * b
    print(f"{name} says {a} times {b} is {product}!")
    return product

multiply(10, 2, "John")
