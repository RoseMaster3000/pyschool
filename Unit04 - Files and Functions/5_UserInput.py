# We have been using BUILT-IN functions for a while now!
# print() for example is a BUILT-IN function
# lets start using input()


print("What is your name?")
name = input()


# input can also be fed the prompt
name = input("What is your name?")


# note that input() alreays returns a STRING
userResponse = input("What is your age?")
age = int(userResponse)
nextYear = age + 1
print(f"You will be {nextYear} next year")


# input VALIDATION refers to checking
# the user's input, and making sure its what you want

while True:
    userResponse = input("What is your first name?")
    if " " in userResponse:
        print("I just want your first name please...")
    else:
        break

# alternatively...

userResponse = " "
while " " in userResponse:
    userResponse = input("What is your first name?")


# alternatively...


def inputFirstName():
    userResponse = input("What is your first name?")
    if " " in userResponse:
        return inputFirstName()
    return userResponse


# input SANATATION refers to modiying
# what data input() gets and cleaning it up
name = input("What is your name?")
name = " ".join(name.strip().title().split())
