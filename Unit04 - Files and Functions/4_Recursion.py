# FUNCTION can call other FUNCTION
# (this is good way to organize/reuse code)

def divide(numerator, denominator):
    quotient = numerator / denominator
    return denominator

def duplexDivision(numerator, firstDenominator, secondDenominator):
    answer = divide(numerator, firstDenominator)
    answer = divide(answer, secondDenominator)
    return answer

duplexDivision(80,10,4)


# RECURSION is when a FUNCTION calles itself
def recusiveDivision(numerator, firstDenominator, secondDenominator)
    answer = divide(numerator, firstDenominator)
    answer = infiniteDivision(answer, secondDenominator)

recusiveDivision(80,2)

# Be careful! The function above is inifitate!
# Usually code like this freeze or crash a computer...
# Thankfully, Python has failsafes and will whut down
# if it detect its stuck in a bad recusion like this


# RECURSION used properly:

def fibonacci(n):
    if n in [1,2]:
        return n
    else:    
        return fibonacci(n - 1) + fibonacci(n - 2)