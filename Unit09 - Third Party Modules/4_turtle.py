# https://studio.code.org/s/coursee-2023/lessons/6/levels/2

from turtle import *
# https://studio.code.org/s/coursee-2023/lessons/6/levels/2
# 
https://docs.python.org/3/library/turtle.html#turtle-methods


import random

def randomColor():
    R = random.randrange(0, 257, 10)
    B = random.randrange(0, 257, 10)
    G = random.randrange(0, 257, 10)
    color(R,G,B)


for c in ['red', 'green', 'blue', 'yellow']:
    color(c)
    forward(75)
    left(90)



