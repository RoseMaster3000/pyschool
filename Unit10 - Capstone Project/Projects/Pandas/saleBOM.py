# add new columns
# using .apply() on dataframe

import pandas as pd
LBS_PER_MTN =  2204.62


###########################################################
# Make sure that BOM_DF and SALES_DF have "LBS Quantity" columns
###########################################################

def convertSalesLBS(row):
    if (row['Sales Unit']=='MT') or (row['Sales Unit']=='MTN'):
        return row['Quantity'] * LBS_PER_MTN
    else: 
        return row['Quantity']
    
def convertBomLBS(row):
    if (row['Un']=='MT'):
        return row['Quantity'] * LBS_PER_MTN
    else:
        return row['Quantity']

###########################################################
# create SALE_COMP_DF, (list of components on sale to sale basis)
###########################################################


# takes a materialCode, returns a list where every item
# is a dict {"Material Code", "Subcomponent Quantity"}
def breakdownComponent(materialCode, quantity, scrapAmount=0):
    subMaterialList = []    
    materialRows = BOM_DF[ BOM_DF["Material"]==materialCode ]

    # No Sub Components
    if (materialRows.shape[0]==0):
        subMaterialRow = {
            "Subcomponent Code": materialCode,
            "Subcomponent Quantity": quantity,
            "Subcomponent Scrap": scrapAmount,
        }
        subMaterialList.append(subMaterialRow)
    
    # Has Sub Components
    else:
        for i,row in materialRows.iterrows():
            # fetch component material
            componentMaterial = row["Component"]

            # calculate component quantity
            salesQuantity = quantity # will be from LBS Quantity 
            bomQuantity = row["LBS Quantity"]  #used to be called "Quantity"
            baseQuantity = row["Base quantity"]
            perPoundQuantity = bomQuantity / baseQuantity
            componentQuantity = perPoundQuantity * salesQuantity

            # account for scrap amount
            scrapPercent = row["C.scrap"]/100
            scrapAmount = componentQuantity * (scrapPercent)
            componentQuantity = componentQuantity + scrapAmount

            # continue breaking down material...
            subMaterialList +=  breakdownComponent(
                componentMaterial,
                componentQuantity,
                scrapAmount
            )

    
    return subMaterialList
        

###########################################################
# Load Dataframes from EXCEL spreadsheets
###########################################################


SALES_DF = pd.read_excel("Sales.xlsx")
# (list of Material sold)
# ['Material code', 'Material text', 'Date', 'Quantity', 'Sales Unit',
# 'Sales document', 'LBS_Converted']

BOM_DF = pd.read_excel("BOM300.xlsx", sheet_name="Template")
# (maps Material to list of Components)
# ['Material', 'Material description', 'BOM Usg', 'AltBOM',
# 'Base quantity', 'BUn', 'UoM', 'Component', 'Comp Description', 'Item',
# 'Quantity', 'Un', 'Fix', 'C.scrap', 'Costing', 'Category',
# 'Category Name']

SALES_DF["LBS Quantity"] = SALES_DF.apply(convertSalesLBS, axis=1)
BOM_DF["LBS Quantity"] = BOM_DF.apply(convertBomLBS, axis=1)



##########################################################################
# ALL SALES COMPONENTS BROKEN DOWN (NO COMPRESSION)
# ALL SALES COMPONENTS BROKEN DOWN + COMPRESSED (on sale basis)
##########################################################################


allMaterialListUncompressed = []
allMaterialListCompressed = []
for i,sale in SALES_DF.iterrows():
    # parse relevent sale values
    saleMaterial = sale["Material code"]
    saleQuantity = sale["LBS Quantity"]
    saleDocument = sale["Sales document"]
    saleCustomer = sale["Sold-to party"]

    # breakdown sale material
    subMaterialList = breakdownComponent(saleMaterial, saleQuantity)

    # add columns to relate breakdown rows to sale
    for subMaterial in allMaterialListUncompressed:
        subMaterial["Original Material"] = saleMaterial
        subMaterial["Original Quantity"] = saleQuantity
        subMaterial["Original Sale Doc"] = saleDocument
        subMaterial["Original Customer"] = saleCustomer

    # add to uncompressed list
    allMaterialListUncompressed += subMaterialList
        
    # compress sale materials rows that share the same "material code"
    subMaterialDict = {}
    for subMaterial in subMaterialList:
        matCode = subMaterial["Subcomponent Code"]
        quant = subMaterial["Subcomponent Quantity"]
        scrap = subMaterial["Subcomponent Scrap"]
        if matCode in subMaterialDict:
            subMaterialDict[matCode]["Subcomponent Quantity"] += quant
            subMaterialDict[matCode]["Subcomponent Scrap"] += scrap
        else:
            subMaterialDict[matCode] = subMaterial

    allMaterialListCompressed += subMaterialDict.values()

    

allMaterialDF = pd.DataFrame(allMaterialListUncompressed)
allMaterialDF.to_csv("sales_uncompressed.csv")

allMaterialDFComp = pd.DataFrame(allMaterialListCompressed)
allMaterialDFComp.to_csv("sales_compressed.csv")


##########################################################################
# Every Sale Material Compressed (overall)
##########################################################################

allMaterialDict = {}
for material in allMaterialListCompressed:
    matCode = material["Subcomponent Code"]
    quant = material["Subcomponent Quantity"]
    scrap = material["Subcomponent Scrap"]
    
    if matCode in allMaterialDict:
        allMaterialDict[matCode]["Subcomponent Quantity"] += quant
        allMaterialDict[matCode]["Subcomponent Scrap"] += scrap
    else:
        allMaterialDict[matCode] = {
            "Subcomponent Code": matCode,
            "Subcomponent Quantity": quant,
            "Subcomponent Scrap": scrap
        }

allMaterialList = allMaterialDict.values()
allMaterialDF = pd.DataFrame(allMaterialList)
allMaterialDF.to_csv("sales_summary.csv")


