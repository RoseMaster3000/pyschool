# read from csv
# enforce expected data types of column


import pandas as pd

def cleanDF(df, typeDictionary):
  # remove duplicates
  df = df.drop_duplicates()
  # drop rows which have non-numeric values in numeric columns
  for col,idealType in typeDictionary.items():
    if (idealType in [int,float]):
      df[col] = pd.to_numeric(df[col], errors='coerce')
  # remove rows with any empty cells
  df.dropna(
    axis=0,
    how="any",
    inplace=True,
    subset = typeDictionary.keys()
  )
  # enfore data types
  df.astype(typeDictionary)
  # return
  return df



df_spark = pd.read_csv("immigration_data_sample.csv")
print(df_spark.shape[0])
print(df_spark["i94yr"].dtype)



immigrationColumnTypes = {
  "cicid": int,
  "i94yr": int,
  "i94mon": int,
  "i94cit": int,
  "i94port": str,
  "i94addr": str,
  "arrdate": int,
  "depdate": int,
  "i94bir": int,
  "i94visa": int,
  "count": int,
  "biryear": int,
  "dtaddto": int,
  "gender": str,
  "airline": str,
  "admnum": int,
  "fltno": int,
  "visatype": str
} 

immigration_df = cleanDF(df_spark, immigrationColumnTypes)
print(immigration_df.shape[0])
print(immigration_df["i94yr"].dtype)





