# A file filled with general purpose utilities
# for IO with files, console, and web fetching
# by Shahrose Kasim (gitlab.com/users/RoseMaster3000)

import os, json, sys, shutil, subprocess
from urllib.request import urlopen
from time import time as epoch
import threading

# TIME
EPOCH_MS = lambda: int(epoch() * 1000)
EPOCH = lambda: int(epoch())

# TERMINAL COLOR CODES
COLOR_ENABLED = True
COLORS = {
    "ENDC":         '\033[0m',
    "BOLD":         '\033[1m',
    "UNDERLINE":    '\033[4m',
    "BLACK":        '\033[0;30m',
    "GRAY":         '\033[1;30m',
    "RED":          '\033[0;31m',
    "LIGHT_RED":    '\033[1;31m',
    "GREEN":        '\033[0;32m',
    "LIGHT_GREEN":  '\033[1;32m',
    "BROWN":        '\033[0;33m',
    "YELLOW":       '\033[1;33m',
    "BLUE":         '\033[0;34m',
    "LIGHT_BLUE":   '\033[1;34m',
    "PURPLE":       '\033[0;35m',
    "LIGHT_PURPLE": '\033[1;35m',
    "CYAN":         '\033[0;36m',
    "LIGHT_CYAN":   '\033[1;36m',
    "LIGHT_GRAY":   '\033[0;37m',
    "WHITE":        '\033[1;37m' ,
}
VERT = '│'
HORZ = '─'
CROS = '┼┌┬┐└┴┘'
########################
# TERMINAL IO
########################

# TERMINAL output
def announce(message, width=40):
    return ANNOUNCE(message, width)
def ANNOUNCE(message, width=40):
    if width<0: width = len(message)*-width
    color("BOLD")
    printLine(width)
    print(message.upper().center(width))
    printLine(width)
    color("ENDC")
def printLine(width=40):
    print("─"*width)
def clear():
    clearScreen()
def clearScreen():
    if os.name == 'nt': os.system('cls')
    else: os.system('clear')
def clearLine(lineCount=1, width=None):
    for i in range(lineCount):
        sys.stdout.write("\033[K")
        sys.stdout.write("\033[F")
        if width!=None:
            if type(width)==str: width = len(width)
            print(" "*width)
            clearLine(lineCount=1, width=None)
def flush():
    sys.stdout.flush()
def shell(commandString):
    commandList = commandString.split()
    commandOutput = subprocess.check_output(commandList)
    return commandOutput.split("\n")
def color(colorKey=""):
    if COLOR_ENABLED and colorKey in COLORS:
        print(COLORS[colorKey], end="")

# prompt the user
def numInput(prompt, minimum=None, maximum=None, integer=False, failed=False):
    # ARGUMENT SANATATION
    if maximum==None: maximum = float('inf')
    if minimum==None: minimum = float('-inf')
    # input prompt (color red if previous attempt failed)
    if failed: color("LIGHT_RED")
    print(prompt, end="")
    if failed: color("ENDC")
    inp = input()

    # blank input (return default / re-display prompt with default)
    if inp =="":
        if minimum == float('-inf') and maximum > 0: default = 0
        else: default = minimum
        clearLine(1)
        print(f"{prompt} {default}")
        return default

    # INPUT VALIDATION
    try:
        # (if previous attempt failed, recolor default color)
        if failed:
            clearLine()
            print(prompt+inp)
        # cast to number
        if integer:
            numericInput = int(inp)
        else:
            numericInput = float(inp)
        # bound checker
        if numericInput > maximum:
            raise Exception("Input value too high")
        elif numericInput < minimum:
            raise Exception("Input value too low")
        else:
            return numericInput
    # on invalid input... (recursively prompt again)
    except:
        clearLine(width=len(prompt+inp))
        return numInput(prompt,
            failed=True,
            maximum=maximum,
            minimum=minimum,
            integer=integer)

# using a list, print numbered menu
def menu(optionList, reverse=False):
    if reverse: optionList = optionList[::-1]
    # generate prompt
    menuLines = []
    width = len(optionList)//10
    for i,option in enumerate(optionList):
        menuLine = "{:>"+str(width)+"}. {}"
        if callable(option): option = option.__name__
        menuLines.append(menuLine.format(i+1, option))
    # display prompt
    print("\n".join(menuLines))
    menuPrompt = f"Select an option (1-{len(optionList)}): "
    menuSelection = -1 + numInput(menuPrompt,
        minimum=1,
        maximum=len(optionList),
        integer=True
        )
    # return item from list
    return optionList[menuSelection]


########################
# FILE IO
########################

# WRITE files
def writeText(content, filePath):
    with open(filePath, "w") as file:
        file.write(content)
def writeJson(data, filePath, indent=4):
    if not filePath.endswith(".json"): filePath += ".json"
    if type(data)==str: data = json.loads(data)
    jsonString = json.dumps(data, indent=indent, sort_keys=False)
    with open(filePath,"w") as file:
        file.write(jsonString)
def renameFile(filePath, newName):
    os.rename(filePath, newName)
def moveFile(fileStart, fileEnd):
    shutil.move(fileStart, fileEnd)

# READ files
def readJson(filePath):
    with open(filePath, "r") as file:
        data = json.load(file)
    return data
def parseJson(jsonString):
    return json.loads(jsonString)
def readText(filePath):
    with open(filePath, "w") as file:
        data = file.read()
    return data
def readData(fileName, outputType=str):
    data=[]
    with open(fileName) as f:
        for line in f:
            if outputType==int:
                data.append(int(line))
            elif outputType==float:
                data.append(float(line))
            else:
                data.append(line)
    return data
def readCSV(filePath, delimiter=",", header=False):
    with open(filePath, "r") as file:
        data = file.read()
        lineList = data.split("\n")
    if header==True:
        headerList = lineList.pop(0).split(delimiter)
        headerList = [h.strip() for h in headerList]
        goodData = []
        for line in lineList:
            entry = {}
            row = line.split(delimiter)
            row = [cell.strip() for cell in row]
            if len(row)!=len(headerList): continue
            for i,key in enumerate(headerList):
                entry[key] = row[i]
            goodData.append(entry)
        return goodData
    else:
        return [line.split(delimiter) for line in lineList]

########################
# FILESYSTEM IO
########################
def isFile(filePath):
    return os.path.isfile(filePath)
def isDirectory(dirPath):
    return os.path.isdir(dirPath)


# DELETE files/folders
def deleteFile(filePath):
    os.remove(filePath)
def deleteDirectory(*dirPath):
    dirPath = joinPath(*dirPath)
    if isDirectory(dirPath):
        shutil.rmtree(dirPath)
def makeDirectory(*dirPath):
    dirPath = joinPath(*dirPath)
    if not isDirectory(dirPath):
        os.mkdir(dirPath)


# LIST folders in directory
def listFolders(*dirPath):
    dirPath = joinPath(*dirPath)
    for file in listDirectory(dirPath):
        path = joinPath(dirPath, file)
        if not isFile(path):
            yield path
# LIST files in directory
def listFiles(*dirPath):
    dirPath = joinPath(*dirPath)
    for file in listDirectory(dirPath):
        path = joinPath(dirPath, file)
        if isFile(path):
            yield path
# LIST files+folders in directory
def listDirectory(*dirPath):
    dirPath = joinPath(*dirPath)
    return os.listdir(dirPath)


# join list of folder into path string
def joinPath(*args):
    if len(args)==0:
        return ""
    elif args[0]==None:
        return os.path.join(*args[1:])
    else:
        return os.path.join(*args)
# clean up string
def sanatizePhrase(phrase):
    for char in list("“”.,\'\"/()[]"):
        phrase = phrase.replace(char," ")
    phrase = " ".join(phrase.split())
    phrase = phrase.title()
    return phrase


########################
# WEB IO
########################
def isWebsite(website):
    try:
        return (urlopen(website).status == 200)
    except:
        return False

# READ webpages
def requestText(website):
    return urlopen(website).read().decode('utf8')
def requestJson(website):
    data = urlopen(website)
    if data==None: return None
    return parseJson(data)


########################
# OTHER
########################

# run a function on a separate thread
def startDaemon(process):
    t = threading.Thread(target=process)
    t.daemon = True
    t.start()

# send an alert popup Zenity (GTK Linux)
def sendAlert(title="Warning",message="An error occurred"):
    command = 'zenity --error --text="{message}\\!" --title="{title}\\!'
    os.system(command)

# send an Y/N popup Zenity (GTK Linux)
def sendQuestion():
    response = os.system('zenity --question --text="Do you wish to continue/?"')
    if response == 0:
        return True
    elif response==256:
        return False
    else:
        return None