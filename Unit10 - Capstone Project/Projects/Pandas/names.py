import pandas as pd
import Levenshtein as lev
from time import time as epoch
EPOCH_MS = lambda: int(epoch() * 1000)
RATIO_LIMIT = 0.7


def alreadyInside(baseList, newItem):
    newItem = newItem.lower().strip()
    for oldItem in baseList:
        ratio = lev.ratio(oldItem, newItem)
        # item match found, item IS inside
        if ratio > RATIO_LIMIT:
            return True, ratio, newItem
    # no item match found, item is NOT inside
    return False, 0.0, newItem 


def mine(df):
    COUNT = 0    
    uniqueNames = []
    goodRows = []
    
    for index, row in df.iterrows():
        name = row['name']
        inside, ratio, item = alreadyInside(uniqueNames, name)
        if not inside:
            uniqueNames.append(item)
            goodRows.append(row)
            row["ratio"] = ratio
            COUNT += 1
            # print(f"{index}\t{COUNT}")

    goodDF = pd.DataFrame(goodRows)
    return goodDF


if __name__=='__main__':

    df = pd.read_csv('test.csv')
    df = df.sort_values(by=['name'], ascending=True)
    df = df.head(1000)
    
    # newDF = yours(df)
    oldTime = EPOCH_MS()
    newDF = mine(df)
    newTime = EPOCH_MS()
    print(newTime-oldTime, "ms")

    newDF.to_csv("output.csv")


