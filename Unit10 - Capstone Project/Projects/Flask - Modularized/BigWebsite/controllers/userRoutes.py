from flask import Flask, render_template, redirect, request
from BigWebsite.models.userData import user
from BigWebsite import app

@app.route('/')
def index_page():
    return redirect('/homepage')

@app.route('/homepage')
def read_page():
    data = user.get_all()
    return render_template("viewAll.html",users = data)

@app.route('/create')
def create_page():
    return render_template('create.html')

@app.route("/account/create",methods =["post"])
def create_account():
    newuser_id = user.insert_user(request.form)
    return redirect(f"/user/{newuser_id}")
    
@app.route("/user/<int:user_id>")
def user_page(user_id):
    data = {"id":user_id}
    userdata = user.get_user(data)
    print(userdata)
    return render_template("viewOne.html",user = userdata)

@app.route("/delete/user/<int:user_id>",methods = ["post"])
def delete(user_id):
    data = {"id":user_id}
    userdata = user.delete_user(data)
    return redirect("/homepage")

@app.route("/edit/<int:user_id>")
def edit_page (user_id):
    data = {"id":user_id}
    userdata = user.get_user(data)
    print(userdata)
    return render_template("edit.html",user = userdata)

@app.route("/account/update/",methods = ["post"])
def update_account():
    newuser_id = user.update_user(request.form)
    return redirect(f"/user/{newuser_id}")


if __name__=="__main__":
    app.run(debug=True)
