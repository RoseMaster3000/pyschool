from BigWebsite import app
from flask_bcrypt import Bcrypt
bcrypt = Bcrypt(app)

#https://werkzeug.palletsprojects.com/en/0.15.x/utils/#module-werkzeug.security
def makeHash(plainText, rounds=8, salt=8):
    return bcrypt.generate_password_hash(
        password = plainText,
        method = f"pbkdf2:sha256:{rounds}",
        salt_length = salt
        )

#https://werkzeug.palletsprojects.com/en/0.15.x/utils/#werkzeug.security.check_password_hash
def checkHash(plainText, hashText):
    return bcrypt.check_password_hash(
        password = plainText,
        pwhash = hashText
        )