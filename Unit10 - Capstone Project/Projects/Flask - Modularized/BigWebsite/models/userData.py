from BigWebsite.configs.SQL import querySQL
from BigWebsite.configs.Security import makeHash, checkHash

class user:
    def __init__( self , data ):
        self.id = data['id']
        self.first_name = data['first_name']
        self.last_name = data['last_name']
        self.email = data['email']
        self.created_at = data['created_at']
        self.updated_at = data['updated_at']

    @classmethod
    def get_all(cls):
        query = "SELECT * FROM users;"
        results = connectToMySQL('users_schema').query_db(query)
        all_users = []
        for row in results:
            new_user = cls(row)
            all_users.append(new_user)
        return all_users

    @classmethod
    def insert_user(cls,data):
        query = "INSERT INTO users (first_name,last_name,email) VALUES (%(first_name)s,%(last_name)s,%(email)s);"
        results = querySQL(query,data)
        return results

    @classmethod
    def get_user(cls,data):
        query = "SELECT * FROM users WHERE id = %(id)s"
        results = querySQL(query,data)
        return cls(results[0])
    
    @classmethod
    def delete_user(cls,data):
        query = "DELETE FROM users WHERE id = %(id)s"
        results = querySQL(query,data)
        return results

    @classmethod
    def update_user(cls,data):
        query = "UPDATE users SET first_name = %(first_name)s, last_name = %(last_name)s,email = %(email)s WHERE users.id = %(id)s"
        results = querySQL(query,data)
        return data["id"]