from prop import Prop
import pygame as pg

vec = pg.math.Vector2

# GLOBAL CONSTANTS (PHYSICS)
HEIGHT = 250
WIDTH = 250
ACC = 0.5
FRIC = -0.20
GRAVITY = 0.2
JUMP = 4.20


class Player(Prop):
    def __init__(self, xPos, yPos, fileName):
        super().__init__(xPos, yPos, fileName)
        self.originalFile = fileName
        self.originalPos = (xPos, yPos)
        self.pos = vec(xPos, yPos)
        self.vel = vec(0, 0)
        self.acc = vec(0, 0)
        self.jumps = 2
        self.left = self.image
        self.right = pg.transform.flip(self.image, True, False)

    def reset(self):
        self.__init__(self.originalPos[0], self.originalPos[1], self.originalFile)

    def jump(self, collisions):
        if self.jumps > 0:
            self.vel.y = -JUMP
            self.jumps -= 1

    def update(self, collisions):
        if len(collisions) > 0:
            # rest jumps when player touchs a platform
            if self.vel.y != -JUMP:
                self.jumps = 2
            # offset player position by however much the platform moved
            collider = collisions[0]
            self.pos.x -= collider.deltaX
            self.pos.y -= collider.deltaY
            # update the position of the sprite
            self.rect.midbottom = self.pos

    def move(self, pressedKeys, collisions):
        # physics and gravity
        self.acc = vec(0, GRAVITY)
        if pressedKeys[pg.K_LEFT]:
            self.acc.x = -ACC
        if pressedKeys[pg.K_RIGHT]:
            self.acc.x = ACC
        if len(collisions) > 0:
            self.pos.y = collisions[0].rect.top + 1
            if not pressedKeys[pg.K_UP]:
                self.vel.y = 0

        # calculate new position (physics simulation)
        self.acc.x += self.vel.x * FRIC
        self.vel += self.acc
        self.pos += self.vel + 0.5 * self.acc

        # left/right screen edges wrap around to eachother
        if self.pos.x > WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = WIDTH

        # top/bottom screen edges wrap around to eachother
        # if self.pos.y > HEIGHT:
        #   self.pos.y = 0
        # if self.pos.y < 0:
        #   self.pos.y = HEIGHT

        # update the position of the sprite
        self.rect.midbottom = self.pos

        # update direction of sprite
        if self.vel.x > 0:
            self.image = self.right
        elif self.vel.x < 0:
            self.image = self.left
