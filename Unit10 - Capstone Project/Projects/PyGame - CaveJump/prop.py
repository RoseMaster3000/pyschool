import pygame as pg


class Prop(pg.sprite.Sprite):
    def __init__(self, xPos, yPos, fileName):
        super().__init__()
        self.image = pg.image.load(f"img/{fileName}.png")
        self.rect = self.image.get_rect()
        self.rect.x = xPos
        self.rect.y = yPos
