from prop import Prop


class Ground(Prop):
    def __init__(self, xPos, yPos, fileName, xBound=0, yBound=0, duration=0):
        super().__init__(xPos, yPos, fileName)
        self.start = (xPos, yPos)
        self.bounds = (xBound, yBound)
        self.cycle = 0
        self.duration = duration
        self.deltaX = 0
        self.deltaY = 0
        self.rect.height = 1

    def update(self, timeDelta):
        if self.duration == 0:
            return

        self.cycle = self.cycle + timeDelta
        if self.cycle > self.duration:
            self.cycle = 0

        progress = self.cycle / self.duration
        if progress > 0.5:
            progress = 1 - progress
        progress *= 2

        newX = int(self.start[0] + self.bounds[0] * progress)
        newY = int(self.start[1] + self.bounds[1] * progress)

        self.deltaX = self.rect.x - newX
        self.deltaY = self.rect.y - newY

        self.rect.x = newX
        self.rect.y = newY
