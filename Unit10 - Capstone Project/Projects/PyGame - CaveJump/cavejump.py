import pygame as pg
import random

from player import Player
from prop import Prop
from ground import Ground


class CaveJump:
    def __init__(self):
        # pygame initialization
        pg.init()
        self.screen = pg.display.set_mode((250, 250))
        pg.display.set_caption("cave jump")
        self.loadImages()
        self.clock = pg.time.Clock()
        self.font = pg.font.Font("badaboom.ttf", 19)
        # game variables
        self.gameOver = False
        self.fps = 60
        self.bgColor = (74, 69, 69)
        self.diamondPos = (100, 100)
        self.charPos = (20, 180)
        self.score = 0
        self.speed = 0
        self.running = True
        self.ms = 0
        self.collisions = []

    def loadImages(self):
        # sprite groups
        self.backgrounds = pg.sprite.Group()
        self.platforms = pg.sprite.Group()
        self.collectables = pg.sprite.Group()
        self.players = pg.sprite.Group()
        # populate background
        self.backgrounds.add(Prop(0, 236, "oj"))
        # populate platforms
        self.platforms.add(Ground(20, 200, "platform2"))
        self.platforms.add(Ground(100, 150, "platform2", 100, 15, 4000))
        self.platforms.add(Ground(100, 120, "platform2"))
        self.platforms.add(Ground(50, 50, "platform2"))
        # populate collectables
        self.spawnDiamond()
        # populate player character
        self.player = Player(20, 180, "player")
        self.players.add(self.player)

    def spawnDiamond(self):
        randomX = random.randint(10, 240)
        randomY = random.randint(10, 200)
        self.collectables.add(Prop(randomX, randomY, "diamond"))

    def clearCollectables(self):
        self.collectables = pg.sprite.Group()

    def updateImages(self):
        self.screen.fill(self.bgColor)
        # draw backgrounds
        self.backgrounds.draw(self.screen)
        # draw platforms
        self.platforms.draw(self.screen)
        self.platforms.update(self.ms)
        # draw collectables
        self.collectables.draw(self.screen)
        # draw player
        self.players.draw(self.screen)
        self.players.update(self.collisions)
        # draw UI text (score)
        self.drawCenteredText(f"Score: {self.score}", 0)
        if self.gameOver:
            self.drawCenteredText("GAME OVER", 95)
            self.drawCenteredText("press R to restart", 130)
            self.drawCenteredText("press ESC to quit", 160)
        # update pygame display
        pg.display.update()

    def drawCenteredText(self, message, yPos):
        fontRender = self.font.render(message, True, (255, 255, 255))
        fontWidth, fontHeight = self.font.size(message)
        xPos = (250 - fontWidth) // 2
        self.screen.blit(fontRender, (xPos, yPos))

    def handleInput(self):
        # key pressing
        pressedKeys = pg.key.get_pressed()
        self.player.move(pressedKeys, self.collisions)
        # key tapping
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    self.quit()
                if event.key == pg.K_r:
                    self.reset()
                if event.key == pg.K_UP and not self.gameOver:
                    self.player.jump(self.collisions)
        # push pygame events
        pg.event.pump()

    def reset(self):
        # reset the game
        self.score = 0
        self.gameOver = False
        self.player.reset()
        self.clearCollectables()
        self.spawnDiamond()
        print("resetting game...")

    def quit(self):
        self.running = False
        print("quitting game...")

    def handlePhysics(self):
        # PLATFORM COLLISION
        self.collisions = pg.sprite.spritecollide(self.player, self.platforms, False)
        # DIAMOND COLLECTION
        self.collections = pg.sprite.spritecollide(
            self.player, self.collectables, False
        )
        if len(self.collections) > 0:
            self.score += 1
            collectedItem = self.collections[0]
            self.collectables.remove(collectedItem)
            self.spawnDiamond()

        # DEATH COLLISION
        self.death = pg.sprite.spritecollide(self.player, self.backgrounds, False)
        if len(self.death) > 0:
            self.gameOver = True

    def run(self):
        while self.running:
            self.handleInput()
            self.handlePhysics()
            self.updateImages()
            self.ms = self.clock.tick(self.fps)
