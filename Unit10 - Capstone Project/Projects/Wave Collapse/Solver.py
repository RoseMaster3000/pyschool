from Soduku import Soduku

class Solver(Soduku):
    # PROPOGATE
    # go over the entire table deleting potentials
    # than can not coexist given the known cells
    # and the constraints (rules of soduku game)
    def revise(self):
        madeProgress = False
        for i in range(self.width):
            for j in range(self.width):
                if self.isSolved(i,j) and not self.propagated[i][j]:
                        self.propagateRow(i,j)
                        self.propagateColumn(i,j)
                        self.propagateSection(i,j)
                        self.propagated[i][j] = True
                        madeProgress = True
        return madeProgress

    def reviseLoop(self):
        while self.revise():
            continue
        return
    
    def propagateRow(self, cursorRow, cursorCol):
        value = self.board[cursorRow][cursorCol][0]
        for j in range(self.width):
            if j != cursorCol:
                if value in self.board[cursorRow][j]:
                    self.board[cursorRow][j].remove(value)

    def propagateColumn(self, cursorRow, cursorCol):
        value = self.board[cursorRow][cursorCol][0]
        for i in range(self.width):
            if i != cursorCol:
                if value in self.board[i][cursorCol]:
                    self.board[i][cursorCol].remove(value)
    
    def propagateSection(self, cursorRow, cursorCol):
        value = self.board[cursorRow][cursorCol][0]
        
        sectionRowStart = int(cursorRow // self.subWidth) * self.subWidth
        sectionRowEnd = sectionRowStart + self.subWidth
        sectionColStart = int(cursorCol // self.subWidth) * self.subWidth
        sectionColEnd = sectionColStart + self.subWidth
        
        for i in range(sectionRowStart,sectionRowEnd):
            for j in range(sectionColStart,sectionColEnd):
                if i!=cursorRow and j!=cursorCol:
                    if value in self.board[i][j]:
                        self.board[i][j].remove(value)


    # ENTROPY
    # create a metadata table that describes how
    # difficult it willbe to solve every cell
    # (aka how many unique potential values can it be)
    def minimum_remaining_values(self):
        entropy = []
        for row in self.board:
            entropyRow = []
            for cell in row:
                entropyRow.append([len(cell)])
            entropy.append(entropyRow)
        return entropy



    def printEntropy(self, indentation):
        entropy = self.minimum_remaining_values()
        Soduku(boardStart=entropy).print(indentation)
