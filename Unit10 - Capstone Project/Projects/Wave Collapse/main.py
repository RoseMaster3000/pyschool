from Soduku import Soduku
from Solver import Solver

def clearScreen():
    import os
    if os.name == 'nt': os.system('cls')
    else: os.system('clear')


if __name__ == "__main__":
    clearScreen()
    # Solver(width=4).print()
    s =Solver(width=9)

    # board = [
    #     [1,None,None,None],
    #     [None,2,None,None],
    #     [None,None,3,None],
    #     [None,None,None,4],
    # ]
    
    # s = Solver(boardStart=board)

    print("TABLE START")
    s.print()
    s.printVerbose()

    s.reviseLoop()
    s.printVerbose()


    # s.printEntropy(1)


    