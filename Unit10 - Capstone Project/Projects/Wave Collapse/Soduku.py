from math import sqrt
VERT = '│'
HORZ = '─'
CROS = '┼'
TOP = '┌┬┐'
BOT = '└┴┘'

class Soduku:
    def __init__(self, width=9, boardStart=None):
        if boardStart:
            if not self.__validateBoard(boardStart):
                raise Exception("Illegal Soduku Starter Board")
            self.board = self.__sanatizeBoard(boardStart)
        else:
            self.board = self.__generateBoard(width)

        # width (width of ENTIRE board)
        self.__width = len(self.board)
        # subWidth (width of board subsections)
        self.__subWidth = sqrt(self.__width)
        if self.__subWidth%1!=0:
            raise Exception("Illegal Soduku Width")
        self.__subWidth = int(self.__subWidth)
        # numWidth (character width of numbers in table)
        self.__numWidth = (self.__width//10)+1


        # metadata
        self.propagated = [[False]*self.width]*self.width

    ###################
    # READ-ONLY ATTRIBUTES
    ###################
    @property
    def width(self):
        return self.__width

    @property
    def subWidth(self):
        return self.__subWidth

    ###################
    # BASIC METHODS
    ###################
    def __repr__(self):
        return str(self)
    
    def __str__(self):
        return f"<Soduku[{self.__width}×{self.__width}] Object>"

    def __getitem__(self, index):
        return self.board[index]

    def set(self, row, column, value):
        self.board[row][column] = value

    def get(self, row, column):
        return self.board[row][column]

    def clone(self):
        return Soduku(boardStart=self.board)

    def isSolved(self, row, column):
        return len(self.get(row,column))==1

    def isPropogated(self, row, column):
        return self.propagated[row][column]

    ###################
    # DISPLAY RENDERING
    ###################
    def print(self, indentation=0):
        display = []
        # handle each row
        for i,row in enumerate(self.board):
            if i%self.__subWidth == 0:
                display.append(self.__makeHorizontal())

            # handle each column (cell)
            cells = []
            for j,cell in enumerate(row):
                if j%self.__subWidth == 0:
                    cells.append(VERT)
                if len(cell)==1:
                    cells.append(f"{cell[0]}")
                else:
                    cells.append("?"*self.__numWidth)
            cells.append(VERT)
            display.append(" ".join(cells))
            
        display.append(self.__makeHorizontal())
        indent = "    " * indentation
        for d in display:
            print(f"{indent}{d}")


    def printVerbose(self, indentation=0):
        display = []
        # handle each row
        for i,row in enumerate(self.board):
            if i%self.__subWidth == 0:
                display.append(self.__makeHorizontal(rowIndex=i))


            # handle each column (cell)
            cells = []
            for j,cell in enumerate(row):
                if j%self.__subWidth == 0:
                    cells.append(VERT)
                jcell = ",".join([str(c) for c in cell])
                jwidth = self.__numWidth
                fcell = f"{jcell:<10}"

                cells.append(str(fcell))

            cells.append(VERT)
            display.append(" ".join(cells))
        
        indent = "    " * indentation
        for d in display:
            print(f"{indent}{d}")


    # generates horizonal dividers for soduku table
    def __makeHorizontal(self, rowIndex):
        subsectionWidth = ((cellWidth)*self.__subWidth)+2
        tableWidth = int(subsectionWidth * self.__subWidth) + 1
        horizontal = ""
        for i in range(tableWidth):
            if i%subsectionWidth==0:
                horizontal += CROS
            else:
                horizontal += HORZ
        return horizontal

    ###################
    # BOARD GENERATION
    ###################
    def __validateBoard(self, board):
        height = len(board)
        for row in board:
            if len(row)!=height:
                return False
        return True

    def __generateBoard(self, width):
        row = [[self.__generateCell(width) for _ in range(width)]]
        return row * width
    
    def __generateCell(self, width):
        return list(range(1, width+1))

    def __sanatizeBoard(self, board):
        cleanBoard = []
        width = len(board)
        for row in board:
            cleanRow = []
            for cell in row:
                if type(cell) is int:
                    cleanRow.append([cell])
                else:
                    cleanRow.append(self.__generateCell(width))
            cleanBoard.append(cleanRow)
        return cleanBoard

