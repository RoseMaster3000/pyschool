import pygame
from os import listdir
from os.path import join as joinPath
from ImageSprite import ImageSprite

class AnimatedSprite(ImageSprite):
    '''
    A class that handles animated pygame sprites.
    It also buffers darkened version of each frame,
    which can be toggled via a public attribute (dark)
    '''

    def __init__(self, x, y, folderName):
        # NORMAL ANIMATION FRAMES
        self.images = []
        fileList = sorted(listdir("assets/"+folderName))
        for fileName in fileList:
            filePath = joinPath("assets", folderName, fileName)
            img = pygame.image.load(filePath)
            self.images.append(img)
        
        # DARKENED ANIMATION FRAMES
        self.dimages = []
        fileList = sorted(listdir("assets/"+folderName))
        for fileName in fileList:
            filePath = joinPath("assets", folderName, fileName)
            img = pygame.image.load(filePath).convert_alpha()
            img.fill((180, 180, 180, 180), special_flags=pygame.BLEND_RGBA_MULT) 
            self.dimages.append(img)

        # BASIC INITIALIZATION
        self.animationLength = len(self.images)
        self.frame = 0
        self.dark = False
        super().__init__(x, y, joinPath(folderName,fileList[0]))

    
    def update(self):
        '''
        For every frame, this function is called
        to update the sprite on screen to display 
        the current variant/frame of the animation.
        '''
        gameFPS = 40
        animFPS = 7
        # increment frame count
        self.frame += animFPS/gameFPS
        if self.frame >= self.animationLength:
            self.frame = 0
        # update image
        if self.dark:
            self.image = self.dimages[int(self.frame)]
        else:
            self.image = self.images[int(self.frame)]


