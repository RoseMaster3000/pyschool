import pygame

class ImageSprite(pygame.sprite.Sprite):
    '''
    A class that handles pygame sprites.
    It is an interface that controls
    the position and surface texture.
    '''
    
    def __init__(self, x, y, filename):
        super().__init__()
        self.loadImage(x, y, "assets/"+filename)

        
    def loadImage(self, x, y, filename):
        '''
        using a file path string, loads
        an image texture to this sprite
        '''
        self.image = pygame.image.load(filename)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y - self.rect.height

    def moveBy(self, dx, dy):
        '''
        Moves the sprite to a new RELATIVE
        position on the screen (delta)
        '''
        self.rect.x += dx
        self.rect.y += dy


    def move(self, ax, ay):
        '''
        Moves the sprite to a new ABSOLUTE
        position on the screen
        '''
        self.rect.x = ax
        self.rect.y = ay


    def printLocation(self, extraInfo=""):
        '''
        Debugging tool that prints the location of this sprite
        into the terminal.
        '''
        print(f"({self.rect.x}, {self.rect.y}) - {extraInfo}")
    
    def detectCollision(self, mousePosition):
        '''
        accepts a tuple (x,y) and returns wheter that
        point collides with this sprite.
        returns a boolean
        '''
        return self.rect.collidepoint(mousePosition)

    # def detectCollision(self, group):
    #     return pygame.sprite.spritecollideany(self, group)