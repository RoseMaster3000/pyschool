import json

class Player:
    '''
    An class that encapualtes one of the two players in a 
    game of Battleship.
    This includes the internal states of their individual boards
    (what spots are occupied by ship, what spots have been shot at)
    '''
    
    ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    LEGAL_ALPHABET = "ABCDEFGHIJ"
    
    def __init__(self, jsonFilePath):        
        self.board = [[False for i in range(10)] for j in range(10)]
        self.shot = [[False for i in range(10)] for j in range(10)]
        self.initFleet(jsonFilePath)
        self.fleetSize = sum(sum(self.board,[]))
    
    def initFleet(self, jsonFilePath):
        '''
        Reads a json file describing a list of boats
        and their attributes (size and location).
        and loads this info into a 2D list (self.board)
        (Ship positions are validated with
        helper functions found below...)
        '''
        with open(jsonFilePath) as file:
            fleetDict = json.load(file)
        for shipIndex, (shipName,shipDict) in enumerate(fleetDict.items()):
            self.getValidShipInfo(shipDict, shipIndex)
            self.placeShip(shipDict)

    def placeShip(self, shipDict):
        '''
        parses a ship location to integer coordinants
        and write True to a 2D list (self.board)
        '''
        x = int(shipDict["x"])-1
        y = self.ALPHABET.index(shipDict["y"])
        size = int(shipDict["size"])
        orientation = shipDict["orientation"]

        if orientation=='h':
            for offset in range(size):
                self.board[y][x+offset] = True
        elif orientation=='v':
            for offset in range(size):
                self.board[y+offset][x] = True


    def printBoard(self):
        '''
        A debugging tool to help visualze the board
        of a Player object (spaces occupied by ships)
        '''
        for row in self.board:
            for cell in row:
                if cell:
                    print(" X ", end="")
                else:
                    print(" . ", end="")
            print()
        
    
    def getValidShipInfo(self, shipDict, shipIndex):
        '''
        Validates ship placement, ensuring there is no overlap
        between multipl ships (taking into account the 
        ships position and size)
        '''
        row = shipDict["x"]
        col = shipDict["y"]
        ori = shipDict["orientation"]

        # validate row is a number from 1-10
        try:
            row = int(shipDict["x"])
        except:
            raise Exception(f"Row value ({row}) in json file is not an integer ")
        if row > 10:
            raise Exception(f"Row value ({row}) in json is too big ")
        elif row < 1:
            raise Exception(f"Row value ({row}) in json is too small")
        row -= 1
            
        # validate col is a letter from A-J 
        if col not in self.ALPHABET:
            raise Exception(f"Column value ({col}) is not a letter.")
        elif col not in self.LEGAL_ALPHABET:
            first = self.LEGAL_ALPHABET[0]
            last = self.LEGAL_ALPHABET[-1]
            raise Exception(f"Column value ({col}) is not a letter between {first} and {last}.")
        col = self.ALPHABET.index(shipDict["y"])

        # validate size
        try:
            size = int(shipDict["size"])
        except:
            raise Exception(f"Size value ({size}) in json file is not an integer.")
        if size < 1:
            raise Exception(f"Size value ({size}) of ship must be 1 or greater.")
            
        # orientation is a character, "h" or "v"
        if ori not in ['h','v']:
            raise Exception(f"Orientation value ({ori}) does not make sense.")
            
        # shipIndex should be a number 0-4 (5 ships total)
        if shipIndex > 5:
            raise Exception(f"Ship Index ({shipIndex}) is too high, 5 ships per game maximum.")

            
        # make sure ship placement is legal (space is not occupied)
        if self.spaceOccupied(row,col,ori,size):
            raise Exception(f"Ship palcement ({row},{col})[{size}] is illegal.")


    # returns True is space is occupied (which is bad)
    def spaceOccupied(self, x, y, orientation, size):
        '''
        A getter method that returns T/F, wheter
        a posion on this player's board is occupied by
        a ship
        '''
        if orientation=='h':
            for offset in range(size):
                if self.board[y][x+offset]:
                    return True
        elif orientation=='v':
            for offset in range(size):
                if self.board[y+offset][x]:
                    return True
        return False

    #####################
    # SHIP GETTER METHODS
    #####################
        
    # mark a space as shot
    # returns True if it hit a spot that has not been shot AND has a ship on it
    def shoot(self, alpha, number):
        '''
        Accepts coordinant (letter,number; NOT number,number)
        Attempts to shoot that coordinant.
        This entails marking a 2D list that keeps track of spots
        which have been shot at (self.shot)
        Returns True if the spot was occupied by an unshot ship ("successful" hit)
        Returns False otherwise ("unsuccessful" hit)
        '''
        j = self.ALPHABET.index(alpha)
        i = number-1
        occupied = self.board[j][i]
        shot = self.shot[j][i]
        
        if (occupied and not shot):
            self.shot[j][i] = True
            # self.fleetSize = sum(sum(self.board,[]))
            self.fleetSize -= 1
            return True
        else:
            self.shot[j][i] = True
            return False

    # returns is space has been shot
    def isShot(self, alpha, number):
        '''
        getter method which check is a specific coordinant
        has been shot at (reads from 2D list self.shot)
        '''
        j = self.ALPHABET.index(alpha)
        i = number-1
        return self.shot[j][i]
        
    # returns if space is occupied
    def isOccupied(self, alpha, number):
        '''
        getter method which check is a specific coordinant
        is occupied by a ship (reads from 2D list self.board)
        '''
        j = self.ALPHABET.index(alpha)
        i = number-1
        return self.board[j][i]



        