import pygame
from ImageSprite import ImageSprite
from AnimatedSprite import AnimatedSprite
from Player import Player
import random

class Battleship:
    '''
    A class that runs an entire game of battleship.
    This uses pygame methods to draw a visualization of both
    players' boards onto the display ("VIEW")
    It also contains 2 player objects where underlying data relevent
    to the game is stored ("MODEL")
    Finally, it handles pygame events to accept input from the human
    player ("CONTROLLER")
    '''
    
    def __init__(self):
        pygame.init()
        pygame.display.set_caption('Battleship!')
        # init attributes
        self._BIGFONT = pygame.font.Font(None, 33)
        self._SMALLFONT = pygame.font.Font(None, 15)
        self._CLOCK = pygame.time.Clock()
        self._FPS = 40
        self._RUNNING = True
        self._ALPHABET = "ABCDEFGHIJ"
        self._DISPLAY = pygame.display.set_mode((800, 500))
        self._TEXT = []
        self._STACK = set()  # positions enemy wants to check
        # init methods
        self.player = Player("player.json")
        self.enemy = Player("enemy.json")
        self.initBoards()
        self.displayBoards()

    def run(self):
        '''
        Main Game Loop, every frame, handles 
        pygame inputs AND draws pygame sprites /
        pygame text to the screen.
        '''
        while self._RUNNING:
            self._DISPLAY.fill((52, 179, 255))
            self.handleInput()
            self.spriteDraw()
            self.textDraw()
            self.spriteUpdate()
            self._CLOCK.tick(self._FPS)

    def gameOver(self, winner):
        '''
        A routine run when either player triggers the lose
        condition (having their fleet be sunk by the enemy)
        '''
        message = f"GAME OVER! {winner.title()} has successfully sunk the enemy fleet!"
        self.textAdd(message, 400, 430)        
        message = f"...press escape to quit..."
        self.textAdd(message, 400, 460)  
        
    ######################
    # Sprite Methods
    ######################
    def spriteUpdate(self):
        '''
        Redraw all the buffered sprites to the screen
        '''
        self._SPRITES.update()
        pygame.display.update()

    def spriteDraw(self):
        '''
        shorthand method for pygame to redraw sprites to
        the screen
        '''
        self._SPRITES.draw(self._DISPLAY)

    def spriteAdd(self, sprite):
        '''
        Adds sprite texture to a buffer
        '''
        self._SPRITES.add(sprite)

    ######################
    # TEXT Methods
    ######################
    def textAdd(self, text, x, y):
        '''
        Adds rendered text texture to a buffer
        '''
        text_surface = self._BIGFONT.render(text, True, (255, 255, 255))
        width, height = self._BIGFONT.size(text)
        textElement = {
            "surface": text_surface,
            "x": round(x - width / 2),
            "y": round(y - height / 2)
        }
        self._TEXT.append(textElement)

    def textDraw(self):
        '''
        Redraw all the buffered text textures to the screen
        '''
        for text in self._TEXT:
            self._DISPLAY.blit(text["surface"], (text["x"], text["y"]))

    ######################
    # Board Methods
    ######################
    def initBoards(self):
        '''
        Initializes 2 dictionaries of EMPTY lists.
        These data structures will be used to store references
        to every pygame tile on the screen.
        '''
        self._PLAYER_BOARD = {}
        self._ENEMY_BOARD = {}
        for alpha in self._ALPHABET:
            self._PLAYER_BOARD[alpha] = [None] * 10
            self._ENEMY_BOARD[alpha] = [None] * 10

    def displayBoards(self):
        '''
        A shorthand method to render / load the tiles
        for both the enemy and player boards
        '''
        self._SPRITES = pygame.sprite.LayeredUpdates()
        self.displaySide(50, 50, "player")
        self.displaySide(440, 50, "enemy")

    def displaySide(self, cornerX, cornerY, side):
        '''
        Creates & buffers every tile element (Animated Sprite)
        for a side of the Battleship game.
        Column / Row text elements are also created & buffered to the screen
        to help make the board more readable.
        '''
        corner = (cornerX, cornerY)
        for i in range(10):
            self.textAdd(str(i + 1), corner[0] + i * 33 + 16, corner[1] - 16)
            for j, alpha in enumerate(self._ALPHABET):
                if (i == 0):
                    self.textAdd(alpha, corner[0] - 16,
                                 corner[1] + j * 33 + 16)
                water = AnimatedSprite(32, 32, "water")
                water.move(corner[0] + i * 33, corner[1] + j * 33)
                self.spriteAdd(water)
                if side == "player":
                    if self.player.board[j][i]:
                        bouy = ImageSprite(32,32,"bouy.png")
                        bouy.move(corner[0] + i * 33, corner[1] + j * 33)
                        self.spriteAdd(bouy)
                    self._PLAYER_BOARD[alpha][i] = water
                else:
                    self._ENEMY_BOARD[alpha][i]  = water

    ######################
    # Handle Enemy Turn (AI)
    ######################
    def executeEnemyTurn(self, letter, number):
        '''
        A method that has the enemy shoot the player
        at a given (letter,number) coordinant
        '''
        hitSuccess = self.player.shoot(letter, number)
        if hitSuccess:
            mark = ImageSprite(32,32,"green.png")
            if (self.player.fleetSize == 0):
                self.gameOver(winner="computer")
            coordSet = self.generateSurrounding(letter, number)
            self._STACK.update(coordSet)
        else:
            mark = ImageSprite(32,32,"red.png")

        tile = self._PLAYER_BOARD[letter][number-1]
        mark.move(tile.rect.x, tile.rect.y)
        self.spriteAdd(mark)

    
    def generateSurrounding(self, letter, number):
        '''
        A method that generates a set of surrounding tile coordinants
        located around the input arguement coordinant (letter, number)
        Tiles that have already been shot are excluded from the set.
        '''
        coordSet = set()
        letterIndex = self._ALPHABET.index(letter)
        boardWidth = len(self._ALPHABET)+1
        firstLetter = self._ALPHABET[0]
        lastLetter = self._ALPHABET[-1]

        # LEFT
        if number != 1:
            prevNumber = number - 1
            if not self.player.isShot(letter, prevNumber):
                coordSet.add(  (letter, prevNumber)  )

        # RIGHT
        if number != boardWidth:
            nextNumber = number + 1
            if not self.player.isShot(letter, nextNumber):
                coordSet.add(  (letter, nextNumber)  )

        # UP
        if letter != firstLetter:
            prevLetter = self._ALPHABET[letterIndex-1]
            if not self.player.isShot(prevLetter, number):
                coordSet.add(  (prevLetter, number)  )

        # DOWN
        if letter != lastLetter:
            nextLetter = self._ALPHABET[letterIndex+1]
            if not self.player.isShot(nextLetter, number):
                coordSet.add(  (nextLetter, number)  )
                
        return coordSet
        
        
    def generateRandom(self):
        '''
        A Method that picks a random spot on the player's
        board that has not been shot
        '''
        coordSet = set()
        letter = random.choice("ABCDEFGHIJ")
        number = random.randint(1,10)
        if self.player.isShot(letter, number):
            return self.generateRandom()
        else:
            coordSet.add((letter, number))
            return coordSet

    
    def smartEnemyTurn(self):
        '''
        Smart AI Implementation that uses a STACK of
        tiles near successful shots
        Algorithm explaination here:
        https://www.datagenetics.com/blog/december32011/index.html
        '''
        if len(self._STACK) == 0:
            self._STACK.update(self.generateRandom())

        letter, number = self._STACK.pop()
        self.executeEnemyTurn(letter, number)
        

    
    ######################
    # Handle Player Input (pygame events)
    ######################
    def handleInput(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                self.handleKeyDown(event)
            elif event.type == pygame.KEYUP:
                self.handleKeyUp(event)
            elif event.type == pygame.MOUSEMOTION:
                self.handleMouseHover(event)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                self.handleMouseDown(event)

    
    def handleKeyDown(self,event):
        if event.key == pygame.K_ESCAPE:
            self._RUNNING = False
    def handleKeyUp(self,event):
        pass

    
    def handleMouseDown(self, event):
        for alpha,row in self._ENEMY_BOARD.items():
            for i,tile in enumerate(row):
                if tile.rect.collidepoint(pygame.mouse.get_pos()):
                    if not self.enemy.isShot(alpha,i):
                        hitSuccess = self.enemy.shoot(alpha, i)
                        if hitSuccess:  
                            mark = ImageSprite(32,32,"green.png")
                            if (self.enemy.fleetSize == 0):
                                self.gameOver(winner="player")
                        else:
                            mark = ImageSprite(32,32,"red.png")
                        mark.move(tile.rect.x, tile.rect.y)
                        self.spriteAdd(mark)
                        # self.randomEnemyTurn()
                        self.smartEnemyTurn()
                        print(self._STACK)
                        return

    
    def handleMouseHover(self,event):                    
        for alpha,row in self._ENEMY_BOARD.items():
            for i,tile in enumerate(row):
                if tile.rect.collidepoint(pygame.mouse.get_pos()):
                    if not self.enemy.isShot(alpha,i):
                        tile.dark = True
                elif tile.dark:
                    tile.dark = False

