'''
The best way to learn is by doing.
Do this cycle as many times as possible
It will give you experience and a collecion
of cool projects.


1) Come up with project idea 
    (make sure the idea is relativley simple/ can be made quickly)
2) Research what packages / modules you need to build it
3) Break project down into parts
4) Develop project 
5) REPEAT STEPS 1-4 with new project idea


DO NOT work on a large project / a project you are attached to
The plan is to build lots of small project so you build skills,
namely archetecture and documentaiton readinng.
Make you mistakes NOW and make them FAST.
By completing lots of small projects quikcly you 
will grow very quickly.

Once you have mastered small projects you can move on to Unit 11
which will discuss strategeis on how to build larger projects.

Alternatively you can move on to Unit 12 if you prefer to 
work on more small projects that solve complex problems.

'''