'''
---------------------------------------
            FINAL PROJECT(S)             
  Using a 3rd Party Module, create an  
     app that you find interesting     
---------------------------------------


        YAGMAIL (python gmail sender)
https://pypi.org/project/yagmail/

        WEB DEVELOPMENT
https://docs.djangoproject.com/en/4.0/intro/tutorial01/
https://flask.palletsprojects.com/en/2.0.x/quickstart/
        will need to learn CSS (Bootstrap/CDNs)
        will need to learn HTML (emmet?)
        will need to learn Data

        DATA ANALYTICS / DASHBOARD
Matplotlib / pandas / pyplot
Ploty Dash (large companies)
Streamlit (fast protyping)
Voila (jupyter integration)
Panel (jupyter integration)

        MEDIA CODING (images, music, videos)
https://earsketch.gatech.edu/landing/#/learn
https://pillow.readthedocs.io/en/stable/
https://docs.opencv.org/3.4/d0/da7/videoio_overview.html

        SOCIAL MEDIA BOTS
https://github.com/InstaPy/InstaPy/blob/master/docs/home.md#installation
https://pydiscord.readthedocs.io/en/latest/quickstart.html
https://python-twitter.readthedocs.io/en/latest/
https://twitchio.readthedocs.io/en/latest/quickstart.html

        CHAT BOTS / TEXTING BOTS
https://www.twilio.com/docs/quickstart/python
https://nlp.johnsnowlabs.com/api/python/getting_started/index.html
https://snips-nlu.readthedocs.io/en/latest/

        GUI (STAND-ALONE PROGRAMS)
https://docs.python.org/3/library/tkinter.html
        https://pythonawesome.com/a-wrapper-around-the-python-tkinter-library-for-customizable-and-modern-ui-elements-in-tkinter/
https://pythonpyqt.com/contents/
        https://doc.qt.io/qtforpython/quickstart.html
        https://doc.qt.io/qt-5/designer-quick-start.html
        pyuic4 input.ui -o output.py

        DATA SCIENCE
https://matplotlib.org/stable/index.html
https://numpy.org/doc/stable/user/absolute_beginners.html

        MACHINE LEARNING
(!) linear algebra experience recommended (!)
https://keras.io/
https://pythonguides.com/tensorflow/

        2D GAME DEVELOPEMENT
https://pygame.readthedocs.io/en/latest/1_intro/intro.html
https://api.arcade.academy/en/latest/
https://www.renpy.org/

        3D GAME DEVELOPEMENT
https://docs.panda3d.org/1.10/python/index
https://docs.panda3d.org/1.10/python/introduction/installation-linux
https://lettier.github.io/3d-game-shaders-for-beginners/index.html
https://github.com/tobspr/RenderPipeline

        DATABASES
https://pymongo.readthedocs.io/en/stable/index.html
http://www.easypythondocs.com/SQL.html

        HOST AN API
https://flask-restful.readthedocs.io/en/latest/quickstart.html

        WEB SCRAPER / CRAWLER
https://selenium-python.readthedocs.io/index.html

        MOUSE + KEYBOARD BOT
https://pyautogui.readthedocs.io/en/latest/quickstart.html
https://pythonhosted.org/pynput/keyboard.html


'''
