# simple input
print("Type your first name:")
first = input()
print(first)


# combine input with IF statement
last = input("Type your last name: ")
if ((last=="Doe") or (last=="Smith")):
    print("Your last name is boring.")

elif last in ["Kasim", "Quasim", "Kaseem"]:
    print("Whoa! Are we realted?!")

else:
    print(f"Cool, your name is {first} {last}.")




# limitations of INPUT (solution in next section...)
age = input("Type your age: ")
age -= 1
print(f"You were {age} last year.")