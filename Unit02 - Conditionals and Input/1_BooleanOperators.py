y = (5 > 6)
print(y)
x = 11


# BOOLEAN OPERATORS
print(x == 11) # True
print(x != 11) # False
print(x > 11)   # False
print(x >= 11)  # True
print(x < 11)   # False
print(x <= 11)  # True


z = 200
# COMPOUND BOOLEAN OPERATORS
print( not (x==11))
print(  (x==11) and (z>100)   )
print(  (x==11) or (z<100)   )
