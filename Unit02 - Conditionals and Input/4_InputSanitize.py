# SANATATION refers to fixing little problems
# with user input before proceeding to program


# Input Sanatation (int)
print("Type your age:")
age = input()
age = int(age)
if age < 0:
    age *= -1
nextAge = age + 1
print(f"You will be {nextAge} next year.")


# Input Sanatation (float)
feet = float(input("Type height ft: "))
print(f"It is {feet} long")


# you can covert back to string as well...
feet = str(feet)
