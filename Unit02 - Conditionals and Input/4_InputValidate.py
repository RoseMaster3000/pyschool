# INPUT VALIDATION refers to rejecting/accepting
# user input / deciding if you even want to go through
# with the program beforehand


print("Type your age:")
age = input()

# sanatize
age = int(age)

# validate
if age <= 0 or age >=110:
    print("Thats not a real age...")
else:
    print(f"Cool, you will be {age+1} next year!")

