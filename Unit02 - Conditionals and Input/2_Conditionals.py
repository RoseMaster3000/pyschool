# IF STATEMENT (parentheses in general is optional)
money = 1000
if (money > 500):
    print("I am rich")


# ELIF (order matters!)
grade = 82
if grade >= 90:
    print("A")
elif grade >= 80:
    print("B")
elif grade >= 70:
    print("C") 
elif grade >= 60:
    print("D") 
elif grade >= 0:
    print("F") 


# ELSE STATEMENT
age = 20
if age >= 21:
    print("You can drink.")
else:
    print("You can not drink.")


# IN STATEMENT (list)
grades = [95,90,100,92,80]
if 80 not in grades:
    print("no 80s here...")
else:
    print("You got an 80")


# IN STATEMENT (string)
message = "hello there!"
if "hello" in message:
    print("hi!")
else:
    print("cool")


# IF + COMPOUNDS (parentheses are now mandatory)
first = "John"
last = "Smith"

if (first=="John") and (last=="Doe" or last=="Smith")
    print("Your name is boring.")
else:
    print("Cool name!")
