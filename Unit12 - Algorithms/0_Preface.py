In a world dominated by GPT, learning algorthms has become very unimportant. 
I personally beleive that mastery of this topic is no longer important.

Hacker rank / coding interviews have been popular in the past, but I
think that this is a thing of the past. Its no longer about mastery, its about 
having enough familiary so that you can utilize GPT to achieve the same 
goals. 

That being said, it is certainly advised to be familar with all the concepts in this section!
In order to prompt GPT you need to know at least the names of algorithms, 
and you also need to be able to organize and integrate the code it provides!