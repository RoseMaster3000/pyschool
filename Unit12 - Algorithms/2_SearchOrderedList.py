# Binary Search Algorithm
##########################
# Find an item in a ORDERED LIST 
# (list is pre-sorted)
# O: log(n)

def search(data, query, offset=0):
    # validate
    length = len(data)
    if length==0: return None
    if length==1 and data[0]!=query: return None

    midIndex = length//2
    midValue = data[midIndex]

    if query == midValue:
        return offset+midIndex
    elif query < midValue:
        return search(data[:midIndex], query, offset)
    elif query > midValue:
        return search(data[midIndex:], query, offset+midIndex)


bank = [2131,22,211,4453,206700,955]
print(search(bank,211))
print(search(bank,206700))
print(search(bank,420))



# normally this is enough:
# print((211 in bank))
# print(bank.index(211))