# REGEX (regular expresssions) is a sequence
# of characters that specifies a
# search pattern in text.

import re

word = "cool"
pattern = r"co*l"

m = re.match(pattern, word)
print(m)

s = re.search(word, pattern, flags=0)
print(s)


def token_count_stats(tokens):
    return descriptive_stats(tokens)[0]


def unique_token_stats(tokens):
    return descriptive_stats(tokens)[1]


def character_count_stats(tokens):
    return descriptive_stats(tokens)[2]


def lexical_diversity_stats(tokens):
    return descriptive_stats(tokens)[3]


lyrics_data["token_count"] = lyrics_data["tokens"].map(token_count_stats)
lyrics_data["unique_tokens"] = lyrics_data["tokens"].map(unique_token_stats)
lyrics_data["character_count"] = lyrics_data["tokens"].map(character_count_stats)
lyrics_data["lexical_diversity"] = lyrics_data["tokens"].map(lexical_diversity_stats)


worstSong = lyrics_data[
    lyrics_data["lexical_diversity"] == min(lyrics_data["lexical_diversity"])
]
worstSong.head()


#######################
# GPT has changed the game with regex. GPT is VERY good at regex.
# Learning it deeply is stupid, familiarity is enough.