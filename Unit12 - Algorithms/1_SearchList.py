# Brute Force Algorithm
# (linear search / sequential search)
##########################
# Find an item in an UNORDERED LIST
# that meets some criterion
# O: n


def search(data, query):
    for i,d in enumerate(data):
        if d == query:
            return i
    return None

bank = [2131,22,211,4453,206700,955]
print(search(bank,22))
print(search(bank,444))
print(search(bank,955))