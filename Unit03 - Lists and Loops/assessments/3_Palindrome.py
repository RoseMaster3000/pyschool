# Check if a string is a palindrome\

word = "racecar"

palindrome = True
for i,c in enumerate(word):
    if word[i] != word[len(word)-1-i]:
        palindrome = False
        break

print(palindrome)
