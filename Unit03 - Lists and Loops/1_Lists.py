# LISTS work just like STRINGS, except
# instead of a sequence of characters
# it can be a sequence of anything you want.
# ...lets make a list of numbers

grades = [95,78,50,68]
firstTest = grades[0]
secondSemester = grades[2:]


# LISTS are ORDERED and MUTABLE
# You can change any ITEM in the LIST
grade[4] = 80




# Using some Python BUILT-IN Functions
# https://docs.python.org/3.8/library/functions.html
gradeSum = sum(grades)
gradeCount = len(grades)
average = gradeSum / gradeCount
print(average)
