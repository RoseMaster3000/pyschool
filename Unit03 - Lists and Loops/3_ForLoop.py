username = "Rose"
usernames = ["Rose", "Lena", "Sunny", "Angela"]


# python - the machine prints R, O, S, E in four
for name in username:
    print(name)


# python - the machine prints rose, lena, sunny, agela
# ⭐ GOOD
for name in usernames:
    print(name)


# python-other ways to print all names in "usernames"
# ⭐ GOOD
for i in range(4):
    print(i)


for i in range(len(usernames)):
    name = usernames[i]
    print(i, name)



# ⭐ GOOD
for i, name in enumerate(usernames):
    print(i, name)


# python while
i = 0
while i < len(usernames):
    print(usernames[i])
    i += 1


# python while break
i = 0
while True:
    if i < len(usernames):
        break
    print(usernames[i])
    i += 1


# javascript / java / C# / C / C++
for(int i=0; i<usernames.length; i++)
{
    name = usernames[i]
    querySQL("INSERT INTO USERS (username, email, password) VALUES (?,?,?);", name, email, password)
}


# javascript for each
usernames.forEach(function(name){
    console.log(name);
});
      




