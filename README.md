# PySchool
PySchool is a collection of introductory coding demonstrations written in Python. These files act as incremental lessons in fundamental programming concepts. They are sorted in an order with ease of learning in mind, and dwell on topics to ensure the learner understands all the implications of new programming concepts as they are introduced.  
*These lessons and programs are covered by a [GPL](https://www.gnu.org/licenses/gpl-3.0.en.html) License. Forks or copies of this code must be made publicly available for free, i.e. Open Source. Alternatively, a commercial license can be purchased from Florasoft upon [negotiation](mailto:rosemaster3000@gmail.com).*

# Table of Contents
#### I. Beginner
0. What is Code?
1. Math and Strings - *Using Python like a calculator*
2. Conditionals and Input - *Using Python as a logic engine*
3. Lists and Loops - *Using Python to do repetitive tasks*
4. FileIO and Functions - *Organizing our Python Code, part 1*

#### II. Intermediate
5. Data Structures - *Storing complex data*
6. Objects - *Organizing our Python Code, part 2*
7. Import: Third Party Modules - *Using python modules from the internet*
8. Import: First Party Modules - *Creating private modules for personal organization*

#### III. Captone Projects
9. Bonus Lessons - *Niche Python Features*
10. Capstone Projects - *Reading Third Party Documentation*


#### V. Advanced 
11. Industry Coding  - *Software Development strategies for large/simple problems*
12. Algorithms - *Software Engineering strategies for small/complex problems*



# Setup
To use this project on Arch Linux:
1. Install Python and git

* <kbd style="background-color: #0078D4; padding: 5px;">Windows</kbd>
Download [Python here](https://www.python.org/downloads/) and [git here](https://git-scm.com/downloads)

* <kbd style="background-color: #E95420; padding: 5px;">Ubuntu</kbd> `sudo snap install python --classic && sudo snap install git
`

* <kbd style="background-color: #CE0058; padding: 5px;">Debian</kbd> `sudo apt update && sudo apt install python3 git`

* <kbd style="background-color: #1793d1; padding: 5px;">Arch</kbd> `sudo pacman -S python git`


2. Clone this repository `git clone https://gitlab.com/RoseMaster3000/pyschool.git`




# Resources
#### Supplemental Challenges
* [Python Books](https://github.com/elngovind/00-PYTHON-BOOKS)
* [Python Exercises](https://www.hackerrank.com/domains/python)
* [Easy Algorithms](https://www.hackerrank.com/domains/algorithms)
* [Hard Algorithms](https://leetcode.com/problemset/all/)
* [Interview Questions](https://www.hackerrank.com/interview/interview-preparation-kit)
* [Python Project Videos](https://www.youtube.com/c/sentdex/playlists)  
* [Design Patterns](https://refactoring.guru/design-patterns/catalog)

#### Further Reading
Mainstream tech news goes for a wider consumer/business demographic. 
Here are better outlets which cater to the engineering crowd.
* [Cybersecurity News](https://www.bleepingcomputer.com/)  
* [Tech Startup News](https://news.ycombinator.com/)  
* [IT News](https://ieeexplore.ieee.org/popular/all)  


#### Useful Downloads
* [My Editor](https://www.sublimetext.com/)
* [My Font](https://rubjo.github.io/victor-mono/)
* [Dyxlexia Font](https://dtinth.github.io/comic-mono-font/)
* [Python Download](https://www.python.org/downloads/)
* [PySchool](https://gitlab.com/RoseMaster3000/pyschool)


## Author
| Shahrose Kasim |             |
|----------------|-------------|
|*[shahros3@gmail.com](mailto:rosemaster3000@gmail.com)*|[shahrose.com](http://shahrose.com)|
|*[rosemaster3000@gmail.com](mailto:rosemaster3000@gmail.com)*|[florasoft.live](https://florasoft.live) |
|*[RoseMaster#3000](https://discordapp.com/users/122224041296789508)*|[discord.com](https://discord.com/)|

