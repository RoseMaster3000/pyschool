# in the "Object Oriented" paradigm, we
# create a collections of IMPERATIVE methods (called classes)
# and we use them to build our program
# attributes are public vs private (externally mutable or immutable)
import WebApp, Security

app = WebApp()
security = Security()
security.setStandard("AES", 128)
app.config["password"] = security
if app.check():
    app.run()


# in the "Functional Programming" paradigm, we
# create a collection of DECLARATIVE functions and use them
# to build our program
# data is exclusively modified by functions (externally immutable)
# and just chain multiple functions to get things done
import Load, Host

settings = Load("settings.ini")
Host.Create().Console().Defualt(settings)
Host.Run()


# you can also split up longer chains accorss multiple lines
station_gb = (
    session.query(
        Measurement.station,
        func.count(Measurement.station)
    )
    .group_by(Measurement.station)
    .order_by(func.count(Measurement.station).desc())
)

station_gb.all()
