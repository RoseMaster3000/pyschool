# Instead of littering your code with print statements
# we can use logging to debug our code
# logging provides us with granular/mature debugging tools

import logging

# create logger with 'some_application'
logger = logging.getLogger("some_application")

# set logger level (https://docs.python.org/3/library/logging.html#levels)
logger.setLevel(logging.DEBUG)

# create file handler (logs written to file)
fh = logging.FileHandler("spam.log")
fh.setLevel(logging.DEBUG)

# create console handler (log written to console)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)



# The assert keyword can be used to debug code.
# The assert keyword lets you test if a condition in your code
# returns True; if it deosn't it raises an "AssertionError"

value = 6 + 4
assert value == 10


# Instead of literring out code with asserts, the
# unittest package can be used to create batches
# of tests that stress test our code

import unittest
class TestStringMethods(unittest.TestCase):
      
    def setUp(self):
        pass
  
    # Returns True if the string contains 4 a.
    def test_strings_a(self):
        self.assertEqual( 'a'*4, 'aaaa')
  
    # Returns True if the string is in upper case.
    def test_upper(self):        
        self.assertEqual('foo'.upper(), 'FOO')
  
    # Returns TRUE if the string is in uppercase
    # else returns False.
    def test_isupper(self):        
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())
  
    # Returns true if the string is stripped and 
    # matches the given output.
    def test_strip(self):        
        s = 'geeksforgeeks'
        self.assertEqual(s.strip('geek'), 'sforgeeks')
  
    # Returns true if the string splits and matches
    # the given output.
    def test_split(self):        
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        with self.assertRaises(TypeError):
            s.split(2)
  
if __name__ == '__main__':
    unittest.main()



# also keep in mind you can throw exceptions intentially
# this can help other developers when they
# start using your code base in incorrect ways
raise ValueError("Parameter too high")

# List of exceptions can be found here
# https://docs.python.org/3/library/exceptions.html

# you can also write code that handles bugs/exceptions
# You should avoid shipping code with this, but 
# there are exceptions to this rule (pun intended)
try:
    raise Exception("Woo crazy bug!")
except Exception as e:
    print(e)
