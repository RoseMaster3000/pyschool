# (MVC) Model View Controller Pattern
# (MTV) Model Template View Pattern (django)
# Chain of Responsibility Pattern
# Modularization (__init__.py)

# Gist
# Singleton
# Factory
# Builder
# Prototype
# Facade
# Command
# Adapter
# Decorator
# Proxy
# Observer
# State
# Strategy
# Template
# Flyweight
