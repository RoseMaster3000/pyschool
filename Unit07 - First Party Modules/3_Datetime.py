from datetime import datetime
#      ^^               ^^
#    module           object


dateObjectA = datetime(month=1, day=15, year=2015)
dateObjectA.strftime("%m/%d/%y")

dateObjectB = datetime.now()
dateObjectB.strftime("%m/%d/%y")

dateObjectC = datetime.strptime("4/26/1997", "%m/%d/%y")
dateObjectC.strftime("%m/%d/%y")



# DATETIME DOCS:
# https://docs.python.org/3/library/datetime.html

# DATE STRING CODES:
# https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes

