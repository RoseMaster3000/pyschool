# Here is a really fun package that comes bundles with Python!

import turtle
  
window = turtle.Screen()
window.bgcolor('black')

pen = turtle.Turtle()
pen.shape('arrow')
pen.color('white')
pen.speed(50)
# pen.tracer(0)


resolution = 120
for j in range(resolution):
    for i in range(4):
        pen.forward(100)
        pen.right(90)
    pen.right(360/resolution)

# pen.tracer(1)
window.exitonclick()