# when learning packages/modules/librariers
# we can use the built-in "inspect" module
# https://docs.python.org/3/library/inspect.html
import inspect


def fun(a):
    return 2*a


print(inspect.isfunction(fun))



class A:
    pass

class B(A):
    pass

class C(B):
    pass

print(inspect.getmro(C))



import collections
print(inspect.ismodule(collections))
print(inspect.ismethod(collections.Counter))
# print(inspect.getmembers(collections))
print(inspect.getmembers(collections, inspect.isfunction))
print(inspect.getmembers(collections, inspect.ismethod))
print(inspect.getmembers(collections, inspect.isbuiltin))