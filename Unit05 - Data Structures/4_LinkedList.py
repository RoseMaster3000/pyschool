# technically, everything in python is an object
# there are no "arrays", instead python List <object>
# the following is more of a simple introduction into
# the more advanced data structures


###########################################
# arrays area a series of data stored adjacent in memory
# fetching data is FAST: just go to the address instantly
# popping/inserting is SLOW (you have to rebuild the entire array)
array = [10,67,30,21]
value = array[2]



###########################################
# linked list is a series of nodes that point to the NEXT node
# fetching data is SLOW: you must traverse O(n)
# popping/inserting is FAST (you just change a couple nodes)


class Node:
    def __init__(self, data):
        self.value = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def __getitem__(self, index):
        return self.fetchIndex(index)

    def __str__(self):
        if self.head==None:
            return "(empty list)"
        else:
            outString = "["
            current = self.head
            while(current.next!=None):
                outString += str(current.value)
                outString += ","
                current = current.next
            outString += str(current.value)
            outString += "]"
            return outString

    # add node to end of LinkedList
    def append(self, newData):
        if self.head == None:
            self.head = Node(newData)
            return
        else:
            current = self.head
            while (current.next!=None):
                current = current.next
            current.next = Node(newData)
            return


    # add node to start of LinkedList
    # sometimes called "push"
    def prepend(self, newData):
        newNode = Node(newData)
        newNode.head = self.head
        self.head = newNode


    # fetch node at index (returns None on failure)
    def fetchIndex(self, index):
        if self.head == None:
            return None

        current = self.head
        while (current!=None):
            if index==0:
                return current
            index -= 1
            current = current.next

        return None


    # delete node with a specified value (return deleted Node)
    def popNode(self, searchValue):
        if self.head==None:
            return None # empty list

        if self.head.value == searchValue:
            oldNode = self.head
            self.head = self.head.next
            return oldNode # head was searched


        current = self.head
        while (current.next!=None):
            if current.next.value == searchValue:
                oldNode = current.next
                current.next = current.next.next
                return oldNode # node within list searched
            current = current.next

        return None


ll = LinkedList()
ll.append(10) # 0
ll.append(67) # 1
ll.append(30) # 2
ll.append(21) # 3

print( ll.fetchIndex(2).value )

ll.popNode(30)
print( ll.fetchIndex(2).value )



###########################################
# *doubly linked list has nodes which point to PERVIOUS and NEXT node
# this takes more memory, but can be handy for certain operations


class DoublyNode:
    def __init__(self, data, previous=None):
        self.value = data
        self.prev = previous
        self.next = None


class DoublyLinkedList:
    def __init__(self):
        self.head = None


    # delete node with a specified value (return deleted Node)
    def popNode(searchValue):
        if self.head==None:
            return None # empty list

        if self.head.value == searchValue:
            oldHead = self.head
            self.head = self.head.next
            self.head.prev = None
            return oldHead # head was searched


        current = self.head
        while (current.next!=None):
            if current.value == searchValue:
                current.prev = current.next
                current.next.prev = current.prev
                return current # node within list searched

        return None # node not found


    # add node to start of LinkedList
    # sometimes called "push"
    def prepend(self, newData):
        newNode = DoublyNode(newData)

        if self.head==None:
            self.head = newNode
        else:
            self.head.prev = newNode
            newNode.next = self.head
            self.head = newNode


    # add node to end of LinkedList
    def append(self, newData):
        if self.head == None:
            self.head = DoublyNode(newData)
        else:
            current = self.head
            while (current.next!=None):
                current = current.next
            current.next = DoublyNode(newData, current)