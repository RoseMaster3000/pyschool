# tuples are just like LISTS
grades = (95,78,50,68)
firstTest = grades[0]
secondSemester = grades[2:]
average = sum(grades) / len(grades)


# TUPLES are ORDER and IMMUTABLE!
# you can't change the ITEMS in a tuple!
grades[0] = 100   # <- throws an error!



# You can, however, MUTATE the entire
# tuple at once
grades = (100,100,100,100)


