# computers are pretty much calculators
# code is us telling the computer to do math


# We can use the Python INTERACTIVE SHELL
# to execute a single line of code and immediatly
# see what it does
8+4
# This is an EXPRESSION
# an EXPRESSION is when you take an OPERATOR (+)
# and manipualte OPERANDS (4 and 8)


# here are many of the BASIC OPERATORS in python
8+4     # addition
8-4     # subtraction
8*4     # multiplication
8/3     # division
8//3    # floor division (rounded down division)
8%3     # modulo (remainder after division)
9**2    # exponent (do not do 10^2)


# there are also BINARY OPERATORS...lets not use those yet
2 ^ 4    # XOR: Each bit of the output is the same as the corresponding bit in x if that bit in y is 0, and it's the complement of the bit in x if that bit in y is 1. 
12 & 4   # AND: Each bit of the output is 1 if the corresponding bit of x AND of y is 1, otherwise it's 0. 
3 | 1    # OR: Each bit of the output is 0 if the corresponding bit of x AND of y is 0, otherwise it's 1.
2 << 7   # Returns x with the bits shifted to the left by y places [x*2**y]
1 >> 13  # Returns x with the bits shifted to the right by y places [x//2**y]
~ 2      # Returns the complement of x, switching each 1 for a 0 and each 0 for a 1 [-x-1]



# INTERACTIVE SHELL will only do 1 line
# to load in multiple lines at once, we use a file...