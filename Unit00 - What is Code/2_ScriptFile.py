# Just code on REPLIT to get started
# But if you want to learn SysAdmin...
# lets make our first python script file1

8+4

# where is the answer?!
# We never told it to *display* the answer

print(8+4)


# print is known as a BUILT-IN function
# its code that has already been written for us

# There are lots of BUILT-INs
# I will show you more when you're ready

# For now, just know that you can use print()
# to display stuff to the screen

