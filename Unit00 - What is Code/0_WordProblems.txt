Before you learn to code, you need to be really good at MATH WORD PROBLEMS.

If you can't do word problems, you can't code. Programmers listen to their mangers project descirptions (word problems), turn those real world ideas into math, and then turn that math into code/algorithms.

If you cant do that first step (word problem interpretation), you cant even begin to do the second step (write code)