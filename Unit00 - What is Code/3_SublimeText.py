# PyCharm is an IDE (lots of bells and whistles to write code)
# Notepad++ is a Text-Editor (just edit text files)


# Sublime Text is a text editor with plugins
# (best of both worlds, simple/fast, but has functionality)
# Atom is an alternative that is similar/Free

# Sublime has a free version with an occasional pop-up.
# https://www.sublimetext.com/download
# https://www.python.org/downloads/


#  [F6] toggles spells check
#  [TAB] is an autocomplete
#  [CTRL]+[SHIFT]+[P] is a menu of everything
#  [CTRL]+B runs the program
#  [CTRL]+[ALT]+[P] change project files (advanced)

#  [CTRL] + [  indent <left
#  [CTRL] + ]  indent right>
#  [CTRL] + ?  comment toggle
#  [CTRL] + L  highlight line
#  [SHIFT] + [ALT] + [🠸⬍🠺]  mutiply cursor


