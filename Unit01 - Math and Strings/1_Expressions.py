dough = 8 + 4
#      ^^^^^^^
#      this "8+4" part is called an EXPRESSION
#      (the part to the right of the "=")


dough = 8 + 4
#         ^
#         the "+" sign is the OPERATOR in this EXPRESSION


dough = 8 + 4
#       ^   ^
#       "8" and "4" are the OPERANDS in this EXPRESSION


dough = 8 + 4
#  ^
#  this is a VARIABLE called "dough"
#  after this EXPRESSION is calculated 
#  the result is stored in "dough"



# There are many OPERATORS (also pemdas exists in python)
answer = 8 + 4 * 2 / (3-2)
print(answer)