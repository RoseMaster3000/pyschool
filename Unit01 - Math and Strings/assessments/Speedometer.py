# THIS ASSIGNMENT USES NO IF STATMENTS
# ITS SOLVABLE WITH VARIABLES and BASIC OPERATORS

# STEP 1: word problem
# My car's speedometer maxes out at 120
# When the car goes faster than 120 mph, the
# speedometer arrow wraps around, starting at 0 again
# My car is going 400 miles per hour
# What number is my speedometer reporting?


# STEP 2: word problem; metacognition
# how did you come to your answer?
# what kind of arithmatic did you use?


# STEP 3 
# we have a car
# right now its going 30 miles per hour (mph)
# store / show me the cars speed
carSpeed = 30
print(f"The car is going {carSpeed} mph")


# STEP 2
# The car sped up by 500 mph! Whoa!
# MUTATE the variable and show me the speed again



# STEP 3
# The speedometer in our car is a circle
# it maxes out at 360, only to wrap around back to 0
# at our current speed, how many times has our speedometer wrapped around
speedometerMax = 360



# STEP 4
# Whar number is our speedometer reporting right now?
# (hint: if our carSpeed was 361, then our speedometer report 1) 

