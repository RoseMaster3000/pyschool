# When doing STRING FORMATTING, we can give additional instructions
# specifying *how* the data should to be injected into the template
# (These are called FORMAT SPECIFICATIONS)

name = "John"
age = 20.456674
height = 6.083333
sex = 'M'
exam = True


print(f"{name:^20}")
print(f"{sex:~^20}")
print(f"{age:<10.2f}{height:>8.0f}ft")
print(f"Final Exam: {str(exam):>8}")
print('~'*20)

number = 103489148129041
print(f"{number:,}")

# for more examples, check the python documentation
# https://docs.python.org/3/library/string.html#format-examples

# string specifiers also described in PEP-3101
# https://peps.python.org/pep-3101/