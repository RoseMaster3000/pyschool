#     INTEGER
# number with no decimal
a = 2

#     FLOAT
# number that has decimals
b = 23.1

#     CHARACTER
# a single letter or symbol
c = 'L'

#     STRING
# is a sequence of CHARACTERS (word, sentence, phrase, etc)
d = "Hello"


# There are many more data types...
# but lets just use these ones for now