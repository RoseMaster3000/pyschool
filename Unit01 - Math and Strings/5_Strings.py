# STRINGS are just sequences of CHARACTERS

stringOne = 'hello'
stringTwo = "welcome"


# less common ways to make strings:
stringThree = '''If you use three
single quotes you can write a
string with line breaks'''

stringFour = """You can also use 
three double quotes to make a 
string with line breaks"""


## NO BREAK STRING
stringFive = (
"this is a a really long "
"string, but it has no "
"breaks.")

stringSix = "this is also "\
    "a long string with "\
    "no line breaks."


print("---")
print(stringFour)
print("---")
print(stringFive)
print("---")


