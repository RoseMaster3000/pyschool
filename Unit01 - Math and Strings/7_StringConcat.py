# multiple STRINGS can be combined into one longer string
# this is referred to as STRING CONCATENATION


# the "+" OPERATOR can CONCATENATE STRINGS
firstName = "Shahrose"
lastName = "Kasim"
space = " "
fullName = firstName + space + lastName
print(fullName)


# the ASSIGNMENT OPERATOR "+=" can CONCATENATE STRINGS
fullName = ""
fullName += "Shahrose"
fullName += " "
fullName += "Kasim"
print(fullName)


# The * OPERATOR can CONCATENATE STRINGS
sing = "la"
yodel = "la" * 8
print(yodel)


# can we CONCATENATE an INTEGER? (no, you can not...)
legacy = 3
space = " "
fullName = firstName + space + legacy
print(fullName)
# what you really want is STRING FORMATTING...


# STRING FORMATTING is a easier to write
# and more powerful than STRING CONCATENATION

# STRING FORMATTING allows us to inject numbers
# (integers/floats) into our strings