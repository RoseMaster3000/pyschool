# declaring a CHARACTER
character = "A"

# CHARACTERS are just numbers in disguise! Lets see his true identity!
# https://www.asciitable.com/
characterUnmasked = ord(character)
print(character)
print(characterUnmasked)


# we can also see what any numbers "CHARACTER" identity is
charSixtyFive = chr(65)
print(charSixtyFive)
