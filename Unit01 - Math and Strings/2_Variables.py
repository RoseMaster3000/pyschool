# this code works, but its very abstract
print(8+4)

# VARIABLES allow us to spread out logic across multiple lines
# The names we choose to use also help make the code more readable
dough = 8 + 4
print(dough)


# this is functionally identical, but VARIABLES makes the code friendlier
# by giving it real-world-context and readability :)
flour = 8
water = 4
dough = water + flour
print(dough)


# code is chronological! it goes from top to bottom
# VARIABLES are MUTABLE: "dough" can be overwritten during RUNTIME
dough = 8 + 4
print(dough)
dough = 200
print(dough)


# code is chronological!
# first the EXPRESSION is evaluated, THEN the answer (over)written to the variable
dough = 10
print(dough)
dough = dough + 2
print(dough)
# use this visualizer if this doesn't make sense...
# https://pythontutor.com/visualize.html#mode=edit


# Lets use an ASSIGNMENT OPERATOR to rewrite the code from above
dough = 10     #  dough = 10
print(dough)   #  print(dough)
dough += 2     #  dough = dough + 2
print(dough)   #  print(dough)


# example of a NameError ("headCount" is undefined)
dough = 12
serving = dough / headCount
print(serving)


# solution to NameError above
headCount = 3
dough = 12
serving = dough / headCount
print(serving)


##########################
# POP QUIZ! What numbers will this print?!
##########################

dough = 12
print(dough)

dough = dough - 2
print(dough)

dough *= 1_000_000
print(dough)

dough /= 5
print(dough)
