# STRING FORMATTING is the process of generating STRING using VARIABLES

# First, we lay out a "template STRING" which has "injection points"
# Then, we inject data into that template's "injection points"

firstName = "Shahrose"
lastName = "Kasim"
legacy = 3

#######################################################
# There are many ways to FORMAT (f-strings are the easiest)
#######################################################



# string FORMATTING with % notation
greeting = "Hi, my name is %s %s %i, where are you?" % (firstName, lastName, legacy)



# string FORMATTING with .format()
greeting = "Hi, my name is {} {} {}, who are you?".format(firstName, lastName, legacy)


# string FORMATTING with f-string (the best way)
greeting = f"Hi, my name is {firstName} {lastName} {legacy}, how are you?"



# string FORMATTING with a DICTIONARY
nameData ={
    "first": "Shahrose", 
    "last": "Kasim",
    "legacy": 3
}
greeting = "My name is {first} {last} {legacy}, when are you?".format(**nameData)

greeting = 'My name is %(first)s %(last)s %(legacy)i, why are you?' % nameData






