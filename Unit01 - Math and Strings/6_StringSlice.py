# STRINGS are just sequences of CHARACTERS
# we are able to "extract" CHARACTERS from STRINGS

name = "Shahrose"
firstLetter = name[0]
fifthLetter = name[4]
lastLetter = name[-1]
# SAME AS >> lastLetter = name[7]
# SAME AS >> lastLetter = name[len(name)-1]

print(name)
print(firstLetter)
print(fifthLetter)
print(lastLetter)


# sub-strings can be extracted from strings (called SLICING)
nameStart = name[:4]  # SAME AS >> nameStart = name[0:4]
nameEnd = name[4:]  # SAME AS >> nameEnd = name[4:8]
nameMid = name[2:6]

print(nameStart)
print(nameEnd)
print(nameMid)
