from random import randint, choice
HEADCOUNT = 15
CONFETTI = list("🎊🎉🪅🎈🥳🍹🍸✨")

def funfetti(width):
    confettiString = ""
    for i in range(width):
        emoji = choice(CONFETTI)
        confettiString += emoji
    print(confettiString)

def surprise(message):
    funfetti(10)
    print(message)
    funfetti(10)

surprise("Alright! Lets throw a party!")