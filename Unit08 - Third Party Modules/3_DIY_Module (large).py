# Some modules are WAY to big to fit into one single file.
# We can turn an entire FOLDER into a python module
# To do this simply create a file called "__init__.py" inside of a folder
# and BOOM now its a module:

from BattleModule import player, enemy

player.attack()
player.attack()
player.attack()

enemy.status()
player.eat()