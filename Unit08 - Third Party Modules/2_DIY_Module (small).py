# Import can be used to bring in code from other files

import ArithmeticModule

# pretty much, we now have a VARIABLE called "ArithmeticModule"...
# this "ArithmeticModule" VARIABLE is an object...
# functions inside "ArithmeticModuleModule.py" are the methods of this "ArithmeticModule" VARIABLE 
# variables inside "ArithmeticModuleModule.py" are the attributes of this "ArithmeticModule" VARIABLE 

# effectivly, we have created our own module called "ArithmeticModule" and are now using it
# (this is pretty much how modules downloaded from pip work, they are just files with 
# code we are borrowing)


message = f"The value of π is {ArithmeticModule.PI:.04f}"
answer = ArithmeticModule.subtract(80,20)


# import can also change the exact VARIABLE name used
import ArithmeticModule as nerd
answer = nerd.divide(8,2)



# import can also be used selectively
# you can precisely import the one function you need
from ArithmeticModule import multiply
answer = multiply(20,3)



# demonstration of multiple concepts at once...
from ArithmeticModule import add as calculateSum
answer = calculateSum(7,3)



#######################################

# You can also import with a WILDCARD
# but I generally advise against this

from ArithmeticModule import *
area = PI * 4
answer = subtract(area, 20)


#######################################
# when your projects get large its a good idea to 
# break your code into logical chunks (separate files)
# and then to import them all together in a "main.py"
