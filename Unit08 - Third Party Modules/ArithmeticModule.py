from random import sample

PI = 3.14159265358979323846264338327950


def subtract(minuend, subtrahend):
    difference = minuend - subtrahend
    return difference

def add(firstAddend, secondAddend):
    summation = firstAddend + secondAddend
    return summation

def divide(numerator, denominator):
    quotient = numerator / denominator
    return quotient

def multiply(multiplier, multiplicand):
    product = multiplier * multiplicand
    return product






