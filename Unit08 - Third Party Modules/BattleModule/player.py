from MyModule import food
from MyModule.enemy import horde

health = 20
sword = 3
hunger = 10


def damageRoll():
    return sword*(health/2)


def attack():
    hunger -= 1
    print("Select target")
    print(horde)
    target = int(input(">"))
    remainingHP = horde[target] - damageRoll()
    remainingHP = max(0,remainingHP)
    horde[target] = remainingHP


def eat():
    if food > 0:
        food -= 1
        hunger += 5
        hunger = min(hunger, 10)
        print("yummy, you eat food")
    else:
        print("Oh no! We are out of food!")