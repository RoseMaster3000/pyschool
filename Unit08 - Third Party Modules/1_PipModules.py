# Most packages must be downloadeded
# and installed from the internet

# These are "3rd-party" packages,
# since they are not "offical python" packages

# Python has a tool called "pip" that helps
# you manage your 3rd-party packages

# Here is a cool package I found on pypi
# It has code from Google that reads text from images
# https://pypi.org/project/pytesseract/

import pytesseract


# First, lets try to use it on an image
# of you own handwriting


#########################################


# This is a site where people's
# screenshots get leaked...
# (its likely a screenshot app they use)
# https://prnt.sc/aa0002


# Lets write a program that downloads all the pictures
# Then lets use pyutresseract to convert it all to text
# Then lets use that metadata to find interesting pictures


#######################################


# With Third party modules, the sky is the limit.
# Leanring how to find / read the documentation of these 
# packages will allow you to build any project you dream of
# In combination with GPT, even large projects can be produced
